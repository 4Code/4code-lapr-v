/*http://www.swi-prolog.org/pldoc/doc_for?object=section(2,'F.1',swi('/doc/Manual/predsummary.html'))*/

:-dynamic liga/2.
liga2(I,F):-liga(I,F);liga(F,I).

criaLabirinto(I,F):-assertz(liga(I,F)).

pista([(Xp,_),(Xi,_)|_], Pista):- Xp > Xi, Pista = 'Esquerda', !.
pista([(Xp,_),(Xi,_)|_], Pista):- Xp < Xi, Pista = 'Direita', !.
pista([(_,Yp),(_,Yi)|_], Pista):- Yp > Yi, Pista = 'Cima', !.
pista([(_,Yp),(_,Yi)|_], Pista):- Yp < Yi, Pista = 'Baixo', !.


labirinto((Xi,Yi),(Xf,Yf),Pista):- lab((Xf,Yf),[[(Xi,Yi)]],Caminho), pista(Caminho,Pista).
lab((Xf,Yf), [[(Xf,Yf)|Temp]|_], [(Xf,Yf)|Temp]):- !.
lab((Xf,Yf), [[(Xi,Yi)|Temp]|Outros], Caminho):-
	findall([(Xaux,Yaux),(Xi,Yi)|Temp], (liga2((Xi,Yi),(Xaux,Yaux)), not(member((Xaux,Yaux), Temp))), L),
	append(Outros,L,LTemp),
	lab((Xf,Yf), LTemp, Caminho).



/*
% pL(X,LR). Pesquisa em Largura
pL(I,F,LR):- pL1(F,[[I]],LR).
pL1(F,[[F|T]|_],[F|T]):- !.
pL1(F,[[C|T]|O],LR):- findall([AUX,C|T], (liga2(C,AUX), not(member(AUX, T))), L), append(O,L,NL), pL1(F,NL,LR).
*/
