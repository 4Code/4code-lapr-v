escreveLista([]). escreveLista([I|F]):-write(I), write(' '), escreveLista(F).
criaAposta(0, []). criaAposta(N, ['_'|F]):- M is N-1, criaAposta(M, F).

getPalavra(Pista,Palavra):-
	random(0,6,N),
	getPalavra(N,Pista,Palavra,
		   [('planeta','terra'),
		    ('agua','mar'),
		    ('agua','potavel'),
		    ('letras','palavra'),
		    ('escrever','caneta'),
		    ('peixe','pescar')]).

getPalavra(0,Pista,Palavra,[(A,B)|_]):-
	random(0,2,X),
	((X==0, Pista = A, Palavra = B);
	(Pista = B, Palavra = A)).
getPalavra(N,Pista,Palavra,[_|Outras]):- M is N-1, getPalavra(M,Pista,Palavra,Outras).
/* para cada caracter corre o predicado verificaLetras(Letra, Solucao[caracter], Adivinha[caracter], Resultado[caracter])*/
verifica(Solucao, Adivinha, Letra, Resultado) :-
     maplist(verificaLetras(Letra), Solucao, Adivinha, Resultado), !.
verificaLetras(Letra, Solucao, Adivinha, Resultado) :-
    (Letra == Solucao, Resultado = Solucao); (Resultado = Adivinha).

terminou(L1, Lista):- reverse(Lista, L2), reverse(L1, L2).

forca(0,_,_):-!, writeln('Perdeu').
forca(Morre, LPalavra, LAposta):-
	escreveLista(LAposta),
	writeln('\nEscreva a sua aposta : '),
	read(C),
	verifica(LPalavra, LAposta, C, LAposta2),
	((terminou(LPalavra, LAposta2), !, escreveLista(LAposta2), write(' Ganhou '));
	((terminou(LAposta, LAposta2), !, M is Morre-1, forca(M, LPalavra, LAposta2)); forca(Morre, LPalavra, LAposta2))).

hangman(Nome):-
	getPalavra(Pista,Palavra),
	write('Pista = '),
	writeln(Pista),
	atom_chars(Palavra, LPalavra),
	length(LPalavra, N),
	criaAposta(N, LAposta),
	forca(6, LPalavra, LAposta),
	writeln(Nome).
