#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define DEBUG 1

using namespace std;

/* VARIAVEIS GLOBAIS */
typedef struct {
	GLboolean		up;
	GLboolean		down;
	GLboolean		left;
	GLboolean		right;
}Teclas;

typedef struct {
	GLboolean   doubleBuffer;
	GLint       delay;
	Teclas		teclas;
}Estado;

typedef struct {
	GLint		x;
	GLint		y;
}Point;

typedef struct {
	GLint		altura;
	GLint       largura;
	Point		posicao;
}Modelo;

Estado estado;
Modelo modelo;
char **labirinto;


/* Inicializa��o do ambiente OPENGL */
void Init(void)
{
	//delay para o timer
	estado.delay=100;

	string line;
	ifstream myfile ("lab.lab");
	if (!myfile.is_open()){
		cout << "Erro ao abrir o ficheiro\n";
		return;
	}
	getline (myfile,line);
	modelo.altura = stoi(line);
	getline (myfile,line);
	modelo.largura = stoi(line);

	labirinto = (char**) malloc(modelo.altura*sizeof(char*));
	for(int i=0; i<modelo.altura; i++){
		labirinto[i] = new char[modelo.largura];

		getline (myfile,line);
		for(int j=0; j<modelo.largura; j++){
			labirinto[i][j] = line[j];
			if(line[j] == 'i'){
				modelo.posicao.x = j;
				modelo.posicao.y = i;
			}
		}
	}
	myfile.close();


	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);

}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint) width, (GLint) height);


	// Matriz Projec��o
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	gluOrtho2D(0, modelo.largura, modelo.altura, 0);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformac�es dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// ... definicao das rotinas auxiliares de desenho ...

void desenhaCubo(GLfloat r, GLfloat g, GLfloat b){
	glBegin(GL_POLYGON);
	glColor3f( r, g, b );
	glVertex3f( 1, 0, 0 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( 0, 1, 0 );
	glVertex3f( 1, 1, 0 );
	glEnd();
}

void desenhaLabirinto(){

	glPushMatrix();

	for(int x = 0; x < modelo.largura; x++){
		for(int y = 0; y < modelo.altura; y++){
			if(labirinto[y][x] == 'x'){
				glPushMatrix();
				glTranslatef(x,y,0);
				desenhaCubo(0,0,1);
				glPopMatrix();
			}
			if(labirinto[y][x] == 'i'){ //inicio
				glPushMatrix();
				glTranslatef(x,y,0);
				desenhaCubo(1,0,0);
				glPopMatrix();
			}
			if(labirinto[y][x] == 'f'){ //fim
				glPushMatrix();
				glTranslatef(x,y,0);
				desenhaCubo(0,1,0);
				glPopMatrix();
			}
		}
	}
	glPopMatrix();

}

void desenhaJogador(){
	glPushMatrix();
	glTranslatef(modelo.posicao.x,modelo.posicao.y,0);
	desenhaCubo(0.5,0.5,0.5);
	glPopMatrix();
}

// Callback de desenho

void Draw(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	desenhaLabirinto();
	desenhaJogador();

	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}

// ... definicao das rotinas de modelo ...

void Vitoria(int value)
{
	printf("\n!!!! Parabens ganhou o jogo !!!!\n");
	Sleep(1000);
	exit(0);
}

GLboolean detectaColisao(GLint nx,GLint ny){
	/*for(int y=0; y<modelo.altura; y++){
		for(int x=0; x<modelo.largura; x++){
			cout << labirinto[y][x];
		}
		cout << endl;
	}

	printf("labirinto[%d][%d]==%c\n",ny,nx,labirinto[ny][nx]);*/
	if(nx<0 || ny<0 || nx>=modelo.largura || ny>=modelo.altura){
		return GL_TRUE;
	}
	if(labirinto[ny][nx]=='f'){
		modelo.posicao.x = nx;
		modelo.posicao.y = ny;
		glutTimerFunc(200, Vitoria, 0);
		return GL_TRUE;
	}
	if(labirinto[ny][nx]=='x'){
		return GL_TRUE;
	}
	return GL_FALSE;
}

// Callback de temporizador

void Timer(int value)
{
	glutTimerFunc(estado.delay, Timer, 0);
	// ... accoes do temporizador ... 
	
	if(estado.teclas.up == GL_TRUE){
		GLint ny = modelo.posicao.y - 1;
		if(!detectaColisao(modelo.posicao.x, ny)){
			modelo.posicao.y = ny;
		}
	}
	if(estado.teclas.down == GL_TRUE){
		GLint ny = modelo.posicao.y + 1;
		if(!detectaColisao(modelo.posicao.x, ny)){
			modelo.posicao.y = ny;
		}
	}
	if(estado.teclas.left == GL_TRUE){
		GLint nx = modelo.posicao.x - 1;
		if(!detectaColisao(nx, modelo.posicao.y)){
			modelo.posicao.x = nx;
		}
	}
	if(estado.teclas.right == GL_TRUE){
		GLint nx = modelo.posicao.x + 1;
		if(!detectaColisao(nx, modelo.posicao.y)){
			modelo.posicao.x = nx;
		}
	}

	// redesenhar o ecra 
	glutPostRedisplay(); 
}


void imprime_ajuda(void)
{
	printf("Seta para a esquerda - Andar para a esquerda\n");
	printf("Seta para cima - Andar para cima\n");
	printf("Seta para a direita - Andar para a direita\n");
	printf("Seta para baixo - Andar para baixo\n");
	printf("ESC - Sair\n");
}

/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interac��o via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 'h' :
	case 'H' :
		imprime_ajuda();
		break;
	case 27 :
		exit(1);
		break;
	default:
		break;
	}
}

void SpecialKey(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP:
		estado.teclas.up = GL_TRUE;
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_TRUE;
		break;
	case GLUT_KEY_LEFT:
		estado.teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_RIGHT:
		estado.teclas.right = GL_TRUE;
		break;
	}
}

void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP:
		estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_FALSE;
		break;
	case GLUT_KEY_LEFT:
		estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_RIGHT:
		estado.teclas.right = GL_FALSE;
		break;
	}
}

int main(int argc, char **argv)
{
	Init();

	estado.doubleBuffer=GL_TRUE;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(20*modelo.largura, 20*modelo.altura);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);
	if (glutCreateWindow("Labirinto") == GL_FALSE)
		exit(1);

	

	imprime_ajuda();

	// Registar callbacks do GLUT

	// callbacks de janelas/desenho
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	//glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	glutTimerFunc(estado.delay, Timer, 0);


	// COMECAR...
	glutMainLoop();

	return 0;
}