#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>
#include <iostream>

using namespace std;

#define DEBUG 1

/* VARIAVEIS GLOBAIS */

typedef struct {
	GLboolean   doubleBuffer;
	GLint       delay;
}Estado;

typedef struct {
	char jogo[3][3];
	char jogada;
}Modelo;


Estado estado;
Modelo modelo;


/* Inicializa��o do ambiente OPENGL */
void Init(void)
{
	//delay para o timer
	estado.delay=100;
	
	modelo.jogada = 'o';

	modelo.jogo[0][2] = ' ';	modelo.jogo[1][2] = ' ';	modelo.jogo[2][2] = ' ';
	modelo.jogo[0][1] = ' ';	modelo.jogo[1][1] = ' ';	modelo.jogo[2][1] = ' ';
	modelo.jogo[0][0] = ' ';	modelo.jogo[1][0] = ' ';	modelo.jogo[2][0] = ' ';


	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);

}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint) width, (GLint) height);


	// Matriz Projec��o
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	// projec��o ortogonal 2D, com profundidade (Z) entre -1 e 1
	gluOrtho2D(0, 3, 0, 3);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformac�es dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


// ... definicao das rotinas auxiliares de desenho ...

void desenhaCubo(GLfloat r, GLfloat g, GLfloat b){
	glBegin(GL_POLYGON);
	glColor3f( r, g, b );
	glVertex3f( 0.9, 0.1, 0.1 );
	glVertex3f( 0.1, 0.1, 0.1 );
	glVertex3f( 0.1, 0.9, 0.1 );
	glVertex3f( 0.9, 0.9, 0.1 );
	glEnd();
}

void desenhaX(){
	glColor3f(1,0,0);
	glBegin(GL_LINE_STRIP);
		glVertex2f(0.2, 0.2);
		glVertex2f(0.8, 0.8);
	glEnd();
	glBegin(GL_LINE_STRIP);
		glVertex2f(0.2, 0.8);
		glVertex2f(0.8, 0.2);
	glEnd();
}

void desenhaO(){
	glColor3f(0,0,1);
	glTranslatef(0.5, 0.5, 0);
	glutSolidSphere(0.4, 16, 2);
}

void desenhaJogo(){
	for(int x=0; x<3; x++){
		for(int y=0; y<3; y++){
			glPushMatrix();
			glTranslatef(x,y,0);
				switch (modelo.jogo[x][y]) {
					case 'x' :
						glPushName(-1);
						desenhaX();
						glPopName();
						break;
					case 'o' :
						glPushName(-2);
						desenhaO();
						glPopName();
						break;
					default :
						glPushName(x*10+y+1);
							desenhaCubo(0.2, 0.2, 0.2);
						glPopName();
						break;
				}
			glPopMatrix();
		}
	}
}

// Callback de desenho

void Draw(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	glPushMatrix();
		desenhaJogo();
	glPopMatrix();

	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}



// Callback de temporizador

void Timer(int value)
{
	glutTimerFunc(estado.delay, Timer, 0);
	// ... accoes do temporizador ... 



	// redesenhar o ecra 
	glutPostRedisplay(); 
}


void imprime_ajuda(void)
{
	printf("ESC - Sair\n");
}

/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interac��o via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 'h' :
	case 'H' :
		imprime_ajuda();
		break;
	case 27 :
		exit(1);
		break;
	default:
		break;
	}

	if(DEBUG)
		printf("Carregou na tecla %c\n",key);

}

int picking(int x, int y){
	int i, n, objid=0;
	double zmin = 10.0;
	GLuint buffer[100], *ptr;

	glSelectBuffer(100, buffer);
	glRenderMode(GL_SELECT);
	glInitNames();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // guarda a projec��o
		glLoadIdentity();

		GLint vport[4];// se est� no modo picking, l� viewport e define zona de picking
		glGetIntegerv(GL_VIEWPORT, vport);
		gluPickMatrix(x, glutGet(GLUT_WINDOW_HEIGHT)  - y, 4, 4, vport); // Inverte o y do rato para corresponder � jana
		gluOrtho2D(0, 3, 0, 3);

	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	desenhaJogo();

	n = glRenderMode(GL_RENDER);
	if (n > 0)
	{
		ptr = buffer;
		for (i = 0; i < n; i++)
		{
			if (zmin > (double) ptr[1] / UINT_MAX) {
				zmin = (double) ptr[1] / UINT_MAX;
				objid = ptr[3];
			}
			ptr += 3 + ptr[0]; // ptr[0] contem o n�mero de nomes (normalmente 1); 3 corresponde a numnomes, zmin e zmax
		}
	}


	glMatrixMode(GL_PROJECTION); //rep�e matriz projec��o
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	return objid;
}

char proximaJogada(){
	if(modelo.jogada == 'x'){
		modelo.jogada = 'o';
		return modelo.jogada;
	} else {
		modelo.jogada = 'x';
		return modelo.jogada;
	}
}

void carregaObj(int obj){
	switch (obj)
	{
	case 1:
		modelo.jogo[0][0] = proximaJogada();
		break;
	case 2:
		modelo.jogo[0][1] = proximaJogada();
		break;
	case 3:
		modelo.jogo[0][2] = proximaJogada();
		break;
	case 11:
		modelo.jogo[1][0] = proximaJogada();
		break;
	case 12:
		modelo.jogo[1][1] = proximaJogada();
		break;
	case 13:
		modelo.jogo[1][2] = proximaJogada();
		break;
	case 21:
		modelo.jogo[2][0] = proximaJogada();
		break;
	case 22:
		modelo.jogo[2][1] = proximaJogada();
		break;
	case 23:
		modelo.jogo[2][2] = proximaJogada();
		break;
	default:
		break;
	}
}

void mouse(int btn, int state, int x, int y){
	switch(btn) {
		case GLUT_LEFT_BUTTON :
					if(state == GLUT_DOWN){
						carregaObj(picking(x,y)); /* trata o objecto carregado */
					}
				break;
	}
}

int main(int argc, char **argv)
{
	estado.doubleBuffer=GL_TRUE;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(300, 300);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);
	if (glutCreateWindow("Jogo Do Galo") == GL_FALSE)
		exit(1);

	Init();

	imprime_ajuda();

	// Registar callbacks do GLUT

	// callbacks de janelas/desenho
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	glutMouseFunc(mouse);
	//glutKeyboardUpFunc(KeyUp);
	//glutSpecialFunc(SpecialKey);
	//glutSpecialUpFunc(SpecialKeyUp);

	glutTimerFunc(estado.delay, Timer, 0);


	// COMECAR...
	glutMainLoop();

	return 0;
}