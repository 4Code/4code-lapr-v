#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>

#ifdef _WIN32
#include <GL/glaux.h>
#endif

#pragma comment (lib, "glaux.lib")    /* link with Win32 GLAUX lib usada para ler bitmaps */

#define NUM_TEXTURAS				9
#define TEXTURA_CABECA				0
#define TEXTURA_CORPO				1
#define TEXTURA_BRACO_ESQUERDO		2
#define TEXTURA_BRACO_DIREITO		3
#define TEXTURA_PERNA_ESQUERDA		4
#define TEXTURA_PERNA_DIREITA		5
#define TEXTURA_FORCA_CIMA			6
#define TEXTURA_FORCA_MEIO			7
#define TEXTURA_FORCA_BAIXO			8

#define NOME_TEXTURA_CABECA				"Cabeca.bmp"
#define NOME_TEXTURA_CORPO				"Corpo.bmp"
#define NOME_TEXTURA_BRACO_ESQUERDO		"BracoEsquerdo.bmp"
#define NOME_TEXTURA_BRACO_DIREITO		"BracoDireito.bmp"
#define NOME_TEXTURA_PERNA_ESQUERDA		"PernaEsquerda.bmp"
#define NOME_TEXTURA_PERNA_DIREITA		"PernaDireita.bmp"
#define NOME_TEXTURA_FORCA_CIMA			"ForcaCima.bmp"
#define NOME_TEXTURA_FORCA_MEIO			"ForcaMeio.bmp"
#define NOME_TEXTURA_FORCA_BAIXO		"ForcaBaixo.bmp"

#define DEBUG 1

/* VARIAVEIS GLOBAIS */

typedef struct {
	GLboolean   doubleBuffer;
	GLint       delay;
}Estado;

typedef struct {
	GLint		bracoDireito;
	GLint		bracoEsquerdo;
}Boneco;

typedef struct {
	GLuint		texID[NUM_TEXTURAS];
	Boneco		boneco;
}Modelo;


Estado estado;
Modelo modelo;

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{

	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint) width, (GLint) height);


	// Matriz Projec��o
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	// projec��o ortogonal 2D, com profundidade (Z) entre -1 e 1
	gluOrtho2D(-6, 6, -6, 6);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformac�es dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


// ... definicao das rotinas auxiliares de desenho ...

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat numImagems , GLint num)
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);

	glTexCoord2f((1/numImagems)*(num+1),0);
	glVertex3fv(a);

	glTexCoord2f((1/numImagems)*(num),0);
	glVertex3fv(b);

	glTexCoord2f((1/numImagems)*(num),1);
	glVertex3fv(c);
	
	glTexCoord2f((1/numImagems)*(num+1),1);
	glVertex3fv(d);
	glEnd();
}

void desenhaQuadrado(int textura, GLfloat numImagems, GLint num)
{
	GLfloat vertices[][3] = {{-1,-1,0}, {1,-1,0}, {1,1,0}, {-1,1,0}};
	GLfloat normais[][3] = {{0,0,-1}};

	glBindTexture(GL_TEXTURE_2D, textura);
	desenhaPoligono(vertices[1],vertices[0],vertices[3],vertices[2],normais[0],numImagems,num);
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaForca(int textura){
	GLfloat normais[][3] = {{0,0,-1}};
	GLfloat vertices[][3] = { {-1,-1,0}, {1,-1,0}, {1,1,0}, {-1,1,0}};

	glBindTexture(GL_TEXTURE_2D, textura);
	desenhaPoligono(vertices[1],vertices[0],vertices[3],vertices[2], normais[0], 1 , 0);
	glBindTexture(GL_TEXTURE_2D, NULL);
}

// Callback de desenho

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT);


	glPushMatrix();
	
	/*desenho do boneco*/
	glPushMatrix();
		glTranslatef(-0.25,-1,0);
		glPushMatrix();
		glTranslatef(0,3,0);
		desenhaQuadrado(TEXTURA_CABECA + 1, 1, 0);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(0,1,0);
		desenhaQuadrado(TEXTURA_CORPO + 1, 1, 0);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(2,1,0);
		desenhaQuadrado(TEXTURA_BRACO_ESQUERDO + 1, 2, modelo.boneco.bracoEsquerdo);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(-2,1,0);
		desenhaQuadrado(TEXTURA_BRACO_DIREITO + 1, 2, modelo.boneco.bracoDireito);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(1.1,-1,0);
		desenhaQuadrado(TEXTURA_PERNA_ESQUERDA + 1, 1, 0);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(-1.1,-1,0);
		desenhaQuadrado(TEXTURA_PERNA_DIREITA + 1, 1, 0);
		glPopMatrix();
	glPopMatrix();

	/*desenho da forca*/
	glPushMatrix();
		glPushMatrix();
		glTranslatef(-1,4,0);
		glScalef(4,1,1);
		desenhaQuadrado(TEXTURA_FORCA_CIMA + 1, 1, 0);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(-4,0,0);
		glScalef(1,3,1);
		desenhaQuadrado(TEXTURA_FORCA_MEIO + 1, 1, 0);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(-1,-4,0);
		glScalef(4,1,1);
		desenhaQuadrado(TEXTURA_FORCA_BAIXO + 1, 1, 0);
		glPopMatrix();
	glPopMatrix();

	glPopMatrix();


	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}



// Callback de temporizador

void Timer(int value)
{
	glutTimerFunc(estado.delay, Timer, 0);
	// ... accoes do temporizador ... 



	// redesenhar o ecra 
	glutPostRedisplay(); 
}


void imprime_ajuda(void)
{
	printf("ESC - Sair\n");
}

/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interac��o via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 'h' :
	case 'H' :
		imprime_ajuda();
		break;
	default:
		break;
	}

	if(DEBUG)
		printf("Carregou na tecla %c\n",key);

}


///////////////////////////////////
/// Texturas

#ifdef _WIN32

AUX_RGBImageRec *LoadBMP(char *Filename)				// Loads A Bitmap Image
{
	FILE *File=NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
	{
		return NULL;									// If Not Return NULL
	}

	File=fopen(Filename,"r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		return auxDIBImageLoad(Filename);				// Load The Bitmap And Return A Pointer
	}

	return NULL;										// If Load Failed Return NULL
}
#endif


void createTextures(GLuint texID[])
{

#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[1];					// Create Storage Space For The Texture

	memset(TextureImage,0,sizeof(void *)*1);           	// Set The Pointer To NULL
#endif

	glGenTextures(NUM_TEXTURAS,texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_CABECA))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_CABECA]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_CABECA);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_CORPO))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_CORPO]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_CORPO);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_BRACO_ESQUERDO))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_BRACO_ESQUERDO]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_BRACO_ESQUERDO);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_BRACO_DIREITO))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_BRACO_DIREITO]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_BRACO_DIREITO);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_PERNA_DIREITA))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_PERNA_DIREITA]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_PERNA_DIREITA);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_PERNA_ESQUERDA))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_PERNA_ESQUERDA]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_PERNA_ESQUERDA);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_FORCA_CIMA))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_FORCA_CIMA]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_FORCA_CIMA);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_FORCA_MEIO))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_FORCA_MEIO]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_FORCA_MEIO);
		exit(0);
	}
	if (TextureImage[0]=LoadBMP(NOME_TEXTURA_FORCA_BAIXO))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[TEXTURA_FORCA_BAIXO]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else
	{
		printf("Textura %s not Found\n",NOME_TEXTURA_FORCA_BAIXO);
		exit(0);
	}

	glBindTexture(GL_TEXTURE_2D, NULL);
}


/* Inicializa��o do ambiente OPENGL */
void Init(void)
{
	//delay para o timer
	estado.delay=100;

	createTextures(modelo.texID);




	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_TEXTURE_2D);

}

void mexeBoneco(int value){

	if(modelo.boneco.bracoDireito < 1){
		modelo.boneco.bracoDireito++;
	} else {
		modelo.boneco.bracoDireito--;
	}

	if(modelo.boneco.bracoEsquerdo < 1){
		modelo.boneco.bracoEsquerdo++;
	} else {
		modelo.boneco.bracoEsquerdo--;
	}

	glutTimerFunc(1000, mexeBoneco, 0);
}


int main(int argc, char **argv)
{
	estado.doubleBuffer=GL_TRUE;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(300, 300);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);
	if (glutCreateWindow("Forca") == GL_FALSE)
		exit(1);

	Init();

	imprime_ajuda();

	// Registar callbacks do GLUT

	// callbacks de janelas/desenho
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	//glutKeyboardUpFunc(KeyUp);
	//glutSpecialFunc(SpecialKey);
	//glutSpecialUpFunc(SpecialKeyUp);

	glutTimerFunc(estado.delay, Timer, 0);

	glutTimerFunc(1000, mexeBoneco, 0);
	// COMECAR...
	glutMainLoop();

	return 0;
}