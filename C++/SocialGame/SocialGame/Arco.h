#ifndef Arco_
#define Arco_

#include <iostream>
#include <list>

using namespace std;

class Arco
{
private:
	int noi;
	int nof;
	GLfloat peso;
	GLfloat largura;

public:
	Arco();
	Arco(int, int, GLfloat, GLfloat);
	Arco(const Arco & arco);
	~Arco();

	void setNoi(int);
	void setNof(int);
	void setPeso(GLfloat);
	void setLargura(GLfloat);

	int getNoi();
	int getNof();
	GLfloat getPeso();
	GLfloat getLargura();

	Arco getArco();

	void details();
};

Arco :: Arco()
{
}

Arco :: Arco(int _noi, int _nof, GLfloat _peso, GLfloat _largura) 
{
	noi = _noi;
	nof = _nof;
	peso = _peso;
	largura = _largura;
}

Arco :: Arco(const Arco & arco)
{
	noi = arco.noi;
	nof = arco.nof;
	peso = arco.peso;
	largura = arco.largura;
}

Arco :: ~Arco()
{
}

void Arco :: setNoi(int _noi)
{
	noi = _noi;
}

void Arco :: setNof(int _nof)
{
	nof = _nof;
}

void Arco :: setPeso(GLfloat _peso)
{
	peso = _peso;
}

void Arco :: setLargura(GLfloat _largura)
{
	largura = _largura;
}

int Arco :: getNoi()
{
	return noi;
}

int Arco :: getNof()
{
	return nof;
}

GLfloat Arco :: getPeso()
{
	return peso;
}

GLfloat Arco :: getLargura()
{
	return largura;
}

Arco Arco :: getArco()
{
	Arco a = Arco(noi, nof, peso, largura);
	return a;
}

void Arco :: details()
{
	cout << "No inicial = " << noi << endl;
	cout << "No final = " << nof << endl;
	cout << "Peso = " << peso << endl;
	cout << "Largura = " << largura << endl;
}

#endif Arco_