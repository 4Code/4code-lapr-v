#ifndef No_
#define No_

#include <iostream>
#include <list>

using namespace std;

#include "Arco.h"
#include "Tag.h"
#include <GL\glut.h>

class No
{
private:
	int id;
	GLfloat x;
	GLfloat y;
	GLfloat z;
	std::list<Arco> arcos;
	std::list<Tag> tags;

public:
	No();
	No(GLfloat, GLfloat, GLfloat);
	No(const No & no);
	~No();

	void setX(GLfloat);
	void setY(GLfloat);
	void setZ(GLfloat);

	GLfloat getX();
	GLfloat getY();
	GLfloat getZ();

	void setId(int);
	int getId();

	void addArco(Arco a);
	void delArco(Arco a);
	int getNumArcos();
	list<Arco> getArcos();

	void addTag(Tag t);
	void delTag(Tag t);
	int getNumTags();

	No getNo();
	No * getNoArcos();

	void details();
};

No :: No()
{
}

No :: No(GLfloat _x, GLfloat _y, GLfloat _z)
{
	x = _x;
	y = _y;
	z = _z;
}

No :: No(const No & no)
{
	id = no.id;
	x = no.x;
	y = no.y;
	z = no.z;
}

No :: ~No()
{
}

void No :: setX(GLfloat _x)
{
	x = _x;
}

void No :: setY(GLfloat _y)
{
	y = _y;
}

void No :: setZ(GLfloat _z)
{
	z = _z;
}

GLfloat No :: getX()
{
	return x;
}

GLfloat No :: getY()
{
	return y;
}

GLfloat No :: getZ()
{
	return z;
}

void No :: setId(int _id)
{
	id = _id;
}

int No :: getId()
{
	return id;
}

void No :: addArco(Arco a)
{
	arcos.push_back(a);
}

void No :: delArco(Arco a)
{
	//arcos.remove(a);
}

int No :: getNumArcos()
{
	return arcos.size();
}

list<Arco> No :: getArcos()
{
	return arcos;
}

void No :: addTag(Tag t)
{
	tags.push_back(t);
}

void No :: delTag(Tag t)
{
	//tags.remove(t);
}

int No :: getNumTags()
{
	return tags.size();
}

No No :: getNo()
{
	return *this;
}

No * No :: getNoArcos()
{
	No no = No(x, y, z);

	list<Arco>::iterator it_arco;
	for (it_arco = arcos.begin() ; it_arco != arcos.end() ; it_arco++)
	{
		no.addArco(it_arco->getArco());
		cout << no.getNumArcos() << endl;
	}

	/*cout << "asdasasd " << no.getNumArcos() << endl << endl;*/

	return this;
}

void No :: details()
{
	cout << "ID DA ESFERA = " << id << endl;
	cout << "X = " << x << endl;
	cout << "Y = " << y << endl;
	cout << "Z = " << z << endl;
}

#endif No_