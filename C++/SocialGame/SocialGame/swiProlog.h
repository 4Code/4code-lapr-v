#include <SWI-cpp.h> 
#include <iostream> 

using namespace std; 

int PlLabirintoMain(int posicao_x, int posicao_y, int fim_x, int fim_y){
	string aux1 = to_string(posicao_x)+","+to_string(posicao_y);
	const char* posInicial = aux1.c_str();
	string aux2 = to_string(fim_x)+","+to_string(fim_y);
	const char* posFinal = aux2.c_str();
	printf("%s\n",posInicial);
	printf("%s\n",posFinal);

	char* argv[] = {"swipl.dll", "-s", "labirinto.pl", NULL}; 

	PlEngine e(3,argv);
	
	PlTermv av(3); 
	av[0] = PlCompound(posFinal); 
	av[1] = PlCompound(posInicial); 
	PlQuery q("labirinto", av);
	while (q.next_solution()) 
	{ 
		std::system ( "CLS" );
		cout << "Ajuda = " << (char*)av[2] << endl; 
	}
 
	/*PlTermv avv(1); 
	avv[0] = PlCompound("liga((1,0),(0,1))."); 
	PlQuery qq("assertz", avv);
	while (q.next_solution()) 
	{ 
	cout << (char*)av[0] << endl; 
	}*/ 

	/*PlTermv av2(2);
	PlQuery q2("liga", av2);
	while (q2.next_solution()) 
	{ 
	cout << (char*)av2[0] << (char*)av2[1] << endl;
	}*/

	return 0; 
}

int verifica(char _t1[1], char _t2[1], char _t3[1], char _t4[1], char _t5[1], char _t6[1], char _t7[1], char _t8[1], char _t9[1]){
	/* Horizontal */
	if(_t1[0] == _t2[0] && _t2[0] == _t3[0]){
		if(_t1[0] == 'x'){
			return 1;
		}else if(_t1[0] == 'o'){
			return 0;
		}
	}
	if(_t4[0] == _t5[0] && _t5[0] == _t6[0]){
		if(_t4[0] == 'x'){
			return 1;
		}else if(_t4[0] == 'o'){
			return 0;
		}
	}
	if(_t7[0] == _t8[0] && _t8[0] == _t9[0]){
		if(_t7[0] == 'x'){
			return 1;
		}else if(_t7[0] == 'o'){
			return 0;
		}
	}
	/* Vertical */
	if(_t1[0] == _t4[0] && _t4[0] == _t7[0]){
		if(_t1[0] == 'x'){
			return 1;
		}else if(_t1[0] == 'o'){
			return 0;
		}
	}
	if(_t2[0] == _t5[0] && _t5[0] == _t8[0]){
		if(_t2[0] == 'x'){
			return 1;
		}else if(_t2[0] == 'o'){
			return 0;
		}
	}
	if(_t3[0] == _t6[0] && _t6[0] == _t9[0]){
		if(_t3[0] == 'x'){
			return 1;
		}else if(_t3[0] == 'o'){
			return 0;
		}
	}
	/* Diagonal */
	if(_t1[0] == _t5[0] && _t5[0] == _t9[0]){
		if(_t1[0] == 'x'){
			return 1;
		}else if(_t1[0] == 'o'){
			return 0;
		}
	}
	if(_t3[0] == _t5[0] && _t5[0] == _t7[0]){
		if(_t3[0] == 'x'){
			return 1;
		}else if(_t3[0] == 'o'){
			return 0;
		}
	}
	return -1;
}

int PlJogoDoGaloMain(char _t1[1], char _t2[1], char _t3[1], char _t4[1], char _t5[1], char _t6[1], char _t7[1], char _t8[1], char _t9[1]){
	if(verifica(_t1, _t2, _t3, _t4,_t5, _t6, _t7, _t8, _t9) != -1){
		if(verifica(_t1, _t2, _t3, _t4,_t5, _t6, _t7, _t8, _t9) == 1){
			printf("Ganhou o X\n");
			return 1;
		} else {
			printf("Ganhou a O\n");
			return 0;
		}
	}
	
	//string aux1 = to_string(posicao_x)+","+to_string(posicao_y);
	//const char* posInicial = aux1.c_str();
	//string aux2 = to_string(fim_x)+","+to_string(fim_y);
	//const char* posFinal = aux2.c_str();
	//printf("%s\n",posInicial);
	//printf("%s\n",posFinal);

	char t1[2] = {_t1[0]+'\0'};
	char t2[2] = {_t2[0]+'\0'};
	char t3[2] = {_t3[0]+'\0'};
	char t4[2] = {_t4[0]+'\0'};
	char t5[2] = {_t5[0]+'\0'};
	char t6[2] = {_t6[0]+'\0'};
	char t7[2] = {_t7[0]+'\0'};
	char t8[2] = {_t8[0]+'\0'};
	char t9[2] = {_t9[0]+'\0'};

	char* argv[] = {"swipl.dll", "-s", "tictactoe.pl", NULL}; 

	PlEngine e(3,argv);
	
	PlTermv av(10);
	av[0] = PlCompound(t1);
	av[1] = PlCompound(t2);
	av[2] = PlCompound(t3);
	av[3] = PlCompound(t4);
	av[4] = PlCompound(t5);
	av[5] = PlCompound(t6);
	av[6] = PlCompound(t7);
	av[7] = PlCompound(t8);
	av[8] = PlCompound(t9);
	PlQuery q("interface", av);
	q.next_solution();
	std::system ( "CLS" );
	cout << "Tabuleiro = " << (char*)av[9] << endl; 
	
	
	char* resultado = (char*)av[9];
	_t1[0] = resultado[0];
	_t2[0] = resultado[1];
	_t3[0] = resultado[2];
	_t4[0] = resultado[3];
	_t5[0] = resultado[4];
	_t6[0] = resultado[5];
	_t7[0] = resultado[6];
	_t8[0] = resultado[7];
	_t9[0] = resultado[8];
	
	if(verifica(_t1, _t2, _t3, _t4,_t5, _t6, _t7, _t8, _t9) != -1){
		if(verifica(_t1, _t2, _t3, _t4,_t5, _t6, _t7, _t8, _t9) == 1){
			printf("Ganhou o X\n");
			return 1;
		} else {
			printf("Ganhou a O\n");
			return 0;
		}
	}

	return -1;
}