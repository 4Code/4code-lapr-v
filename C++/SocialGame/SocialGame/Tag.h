#ifndef Tag_
#define Tag_

#include <iostream>
#include <string>
#include <list>

using namespace std;

class Tag
{
private:
	string descricao;

public:
	Tag();
	Tag(string);
	Tag(const Tag & tag);
	~Tag();

	void setDescricao(string);

	string getDescricao();

	Tag getTag();

	void details();
};

Tag :: Tag()
{
}

Tag :: Tag(string _descricao)
{
	descricao = _descricao;
}

Tag :: Tag(const Tag & tag)
{
	descricao = tag.descricao;
}

Tag :: ~Tag()
{
}

void Tag :: setDescricao(string _descricao)
{
	descricao = _descricao;
}

string Tag :: getDescricao()
{
	return descricao;
}

Tag Tag :: getTag()
{
	Tag tag = Tag(descricao);
	return tag;
}

void Tag :: details()
{
	cout << "Descricao = " << descricao << endl;
}

#endif Tag_