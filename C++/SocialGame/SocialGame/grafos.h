#ifndef _GRAFO_INCLUDE
#define _GRAFO_INCLUDE

#include <iostream>
#include <fstream>

#include "Arco.h"
#include "No.h"

#define __GRAFO__FILE__ "exemplo.grafo"

/*#define _MAX_NOS_GRAFO 100
#define _MAX_ARCOS_GRAFO 200

#define NORTE_SUL	0
#define ESTE_OESTE	1
#define PLANO		2

typedef struct No{
	float x,y,z,largura;
}No;

typedef struct Arco{
	int noi,nof;
	float peso,largura;
}Arco;

extern No nos[];
extern Arco arcos[];
extern int numNos, numArcos;

void addNo(No);
void deleteNo(int);
void imprimeNo(No);
void listNos();
No criaNo(float, float, float);

void addArco(Arco);
void deleteArco(int);
void imprimeArco(Arco);
void listArcos();
Arco criaArco(int, int, float, float);

void gravaGrafo();
void leGrafo();*/


#define __AFASTAMENTO_COTA__ 8

list<No> nos;
list<Arco> arcos;
int numNos;
int numArcos;
No no_global;
int id_esfera = 0;

void obterNoAtravesPos(int num)
{
	list<No>::iterator it_no = nos.begin();
	for (int i = 0 ; i < num ; i++)
	{
		it_no++;
	}

	//cout << "NUMERO DE ARCOS TO IT_NO->GETNO(): " << it_no->getNumArcos() << endl;

	no_global = (*it_no);

	//cout << "NUMERO DE ARCOS NA VARIAVEL GLOABL: " << no.getNumArcos() << endl;
}

void preencherListas()
{
	list<Arco>::iterator it_arco;
	for (it_arco = arcos.begin() ; it_arco != arcos.end() ; it_arco++)
	{
		Arco a = it_arco->getArco();

		list<No>::iterator it_no;
		
		//verificar qual o no inicial e adicionar na lista de arcos desse no o novo arco
		int j = 0;

		for (it_no = nos.begin() ; it_no != nos.end() ; it_no++)
		{

			if (j == a.getNoi())
			{
				it_no->addArco(a);
			}

			j++;
		}

		//verificar qual o no final e adicionar na lista de arcos desse no o novo arco
		j = 0;

		for (it_no = nos.begin() ; it_no != nos.end() ; it_no++)
		{
			if (j == a.getNof())
			{
				it_no->addArco(a);
			}

			j++;
		}
	}

	/*list<No>::iterator it_no;
	int k = 0;
	for (it_no = nos.begin() ; it_no != nos.end() ; it_no++)
	{
		cout << "No: " << k << " tem " << it_no->getNumArcos() << " arcos" << endl;
		k++;
	}*/
}

void calcularCotas()
{
	list<No>::iterator it_no;
	for (it_no = nos.begin() ; it_no != nos.end() ; it_no++)
	{
		it_no->setZ(it_no->getNumArcos() * __AFASTAMENTO_COTA__);
	}
}

void leGrafo()
{
	ifstream myfile;

	myfile.open (__GRAFO__FILE__, ios::in);
	if (!myfile.is_open())
	{
		cout << "Erro ao abrir " << __GRAFO__FILE__ << " para ler" <<endl;
		exit(1);
	}
	
	myfile >> numNos;

	for(int i = 0 ; i < numNos ; i++)
	{
		float x = 0.0;
		float y = 0.0;
		float z = 0.0;

		No no = No(x, y, z);
		no.setId(id_esfera);
		id_esfera++;
		nos.push_back(no);
	}
		
	myfile >> numArcos;

	for(int i = 0 ; i < numArcos ; i++)
	{
		int noi, nof;
		float peso, largura;
		myfile >> noi >> nof >> peso >> largura;
		Arco arco = Arco(noi, nof, peso, largura);
		arcos.push_back(arco);
	}

	/*cout << "Foram criados " << nos.size() << " nos" << endl;
	cout << "Foram criados " << arcos.size() << " arcos" << endl;*/
		
	myfile.close();

	preencherListas();

	calcularCotas();
}

int calcularEsferasNivel(int nivel)
{
	int cont = 0;
	list<No>::iterator it_no;
	for (it_no = nos.begin() ; it_no != nos.end() ; it_no++)
	{
		//esta no mesmo nivel
		if (it_no->getZ() == nivel * __AFASTAMENTO_COTA__)
		{
			cont++;
		}
	}

	return cont;
}


//#include "grafos.h"
//#include <iostream>
//#include <fstream>
//
//#define __GRAFO__FILE__ "exemplo.grafo"
//
//No nos[_MAX_NOS_GRAFO];
//Arco arcos[_MAX_ARCOS_GRAFO];
//int numNos=0, numArcos=0;
//
//using namespace std;
//
//void addNo(No no){
//	if(numNos<_MAX_NOS_GRAFO){
//		nos[numNos]=no;
//		numNos++;
//	}else{
//		cout << "Numero de nos chegou ao limite" << endl;
//	}
//}
//
//void deleteNo(int indNo){
//	if(indNo>=0 && indNo<numNos){
//		for(int i=indNo; i<numNos; nos[i++]=nos[i+i]);
//		numNos--;
//	}else{
//		cout << "Indice de no invalido" << endl;
//	}
//}
//
//void imprimeNo(No no){
//	cout << "X: " << no.x << "Y: " << no.y << "Z: " << no.z <<endl;
//}
//
//void listNos(){
//	for(int i=0; i<numNos; imprimeNo(nos[i++]));
//}
//
//No criaNo(float x, float y, float z){
//	No no;
//	no.x=x;
//	no.y=y;
//	no.z=z;
//	return no;
//}
//
//void addArco(Arco arco){
//	if(numArcos<_MAX_ARCOS_GRAFO){
//		arcos[numArcos]=arco;
//		numArcos++;
//	}else{
//		cout << "Numero de arcos chegou ao limite" << endl;
//	}
//}
//
//void deleteArco(int indArco){
//	if(indArco>=0 && indArco<numArcos){
//		for(int i=indArco; i<numArcos; arcos[i++]=arcos[i+i]);
//		numArcos--;
//	}else{
//		cout << "Indice de arco invalido" << endl;
//	}
//}
//
//void imprimeArco(Arco arco){
//	cout << "No inicio: " << arco.noi << "No final: " << arco.nof << "Peso: " << arco.peso << "Largura: " << arco.largura << endl;
//}
//
//void listArcos(){
//	for(int i=0; i<numArcos; imprimeArco(arcos[i++]));
//}
//
//Arco criaArco(int noi, int nof, float peso, float largura){
//	Arco arco;
//	arco.noi=noi;
//	arco.nof=nof;
//	arco.peso=peso;
//	arco.largura=largura;
//	return arco;
//}
//
//void gravaGrafo(){
//	ofstream myfile;
//
//	myfile.open (__GRAFO__FILE__, ios::out);
//	if (!myfile.is_open()) {
//		cout << "Erro ao abrir " << __GRAFO__FILE__ << "para escrever" <<endl;
//		exit(1);
//	}
//	myfile << numNos << endl;
//	for(int i=0; i<numNos;i++)
//		myfile << nos[i].x << " " << nos[i].y << " " << nos[i].z <<endl;
//	myfile << numArcos << endl;
//	for(int i=0; i<numArcos;i++)
//		myfile << arcos[i].noi << " " << arcos[i].nof << " " << arcos[i].peso << " " << arcos[i].largura << endl;
//	myfile.close();
//}
//void leGrafo(){
//	ifstream myfile;
//
//	myfile.open (__GRAFO__FILE__, ios::in);
//	if (!myfile.is_open()) {
//		cout << "Erro ao abrir " << __GRAFO__FILE__ << "para ler" <<endl;
//		exit(1);
//	}
//	myfile >> numNos;
//	for(int i=0; i<numNos;i++)
//		myfile >> nos[i].x >> nos[i].y >> nos[i].z;
//	myfile >> numArcos ;
//	for(int i=0; i<numArcos;i++)
//		myfile >> arcos[i].noi >> arcos[i].nof >> arcos[i].peso >> arcos[i].largura ;
//	myfile.close();
//	// calcula a largura de cada no = maior largura dos arcos que divergem/convergem desse/nesse no	
//	for(int i=0; i<numNos;i++){
//		nos[i].largura=0;
//		for(int j=0; j<numArcos; j++)
//			if ((arcos[j].noi == i || arcos[j].nof == i) && nos[i].largura < arcos[j].largura)
//				nos[i].largura = arcos[j].largura;
//	}		
//}


#endif