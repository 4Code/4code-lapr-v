﻿
// File generated by Wsutil Compiler version 1.0095 
// This file defines C/C++ functions, callbacks, types that correspond to operations,
// and types specified in WSDL and XSD files processed by Wsutil compiler. The generated 
// type definitions, function and callback declarations can be used with various 
// Web Services APIs either in applications that send and receive requests to and 
// from a running web service, or in the implementation of web services, or in both. 
//
// If Wsutil has generated this file from a WSDL file, the file may contain up to three 
// sets of C/C++ definitions. The first set consists of function declarations that can be 
// used in calling operations on a running web service from client applications. 
// The second set consists of callback declarations that can be used in the 
// implementation of web services.  
// The third set contains definitions C/C++ structures that are used for serialization of 
// C/C++ types to Schema types defined in the WSDL file.
//
// The set of function declarations for web services clients may contain the 
// following declaration: 
//
//    HRESULT WINAPI DefaultBinding_ICalculator_Add(
//       _In_ WS_SERVICE_PROXY* _serviceProxy,
//       _In_ int a, 
//       _In_ int b, 
//       _Out_ int* result, 
//       _In_ WS_HEAP* _heap,
//       _In_opt_ WS_CALL_PROPERTY* _callProperties,
//       _In_ const ULONG _callPropertyCount,
//       _In_opt_ const WS_ASYNC_CONTEXT*_asyncContext,
//       _In_opt_ WS_ERROR* _error);
//
// This function can be called from an application code to request ICalculator web 
// service to perform operation Add using two integers a and b as input data.
// Once the web service processes this request and sends the result back,
// the result is returned to the caller of this function in the "result" parameter.
// A WS_SERVICE_PROXY must be created using WsCreateServiceProxy() function
// before this proxy function can be invoked.
//
// The set of callbacks for implementation of web services may contain the following 
// definition of a callback: 
//
//    typedef HRESULT (CALLBACK* DefaultBinding_ICalculator_AddCallback) (
//       _In_ const WS_OPERATION_CONTEXT* _context,
//       _In_ int a, 
//       _In_ int b, 
//       _Out_ int* result, 
//       _In_ const WS_ASYNC_CONTEXT* _asyncContext,
//       _In_ WS_ERROR* _error); 
//
//    typedef struct DefaultBinding_ICalculatorFunctionTable 
//    {
//         DefaultBinding_ICalculator_AddCallback DefaultBinding_ICalculator_Add;
//    } DefaultBinding_ICalculatorFunctionTable;
//
// This definition can be used to implement a function that defines the Add operation 
// as a part of an ICalculator web service. Once this callback is implemented and 
// the web service is deployed and running, if a client applications attempts to 
// execute the operation Add, the implementation of this callback is called and 
// it receives two integers a and b as input data and it returns a third integer 
// that is the result of the operation. A WS_SERVICE_ENDPOINT must be filled out, 
// and its contract field set to instance of such a function table.  
// WsCreateServiceHost may then be used to register the endpoint as a service.
// 
// The set with definitions of C/C++ structures may contain a definition of the following 
// structure:
//    typedef struct ContactInfo 
//    {
//			WCHAR*  name;
//			WCHAR*  email;
//			__int64 contactID;
//	  } ContactInfo;
//
// This structure corresponds to the following complex type defined in the WSDL file:
//    <s:complexType name="ContactInfo">
//      <s:sequence>
//        <s:element minOccurs="1" maxOccurs="1" name="name" type="s:string" />
//        <s:element minOccurs="1" maxOccurs="1" name="email" type="s:string" />
//        <s:element minOccurs="1" maxOccurs="1" name="contactID" type="s:long" />
//      </s:sequence>
//    </s:complexType>
//
// For more information about content of this file, please see the documentation for
// Wsutil compiler.
// If Wsutil generates this file from a WSDL file and policy processing is enabled using
// policy switch, the file may contain declaration of functions that can be used 
// for creating a proxy to a web service or an endpoint of a web service. 
// For example, the file may contain declaration of the following function:
//
//    HRESULT DefaultBinding_ICalculator_CreateServiceProxy(
//    _In_ WS_HTTP_SSL_BINDING_TEMPLATE* templateValue,
//    _In_reads_opt_(proxyPropertyCount) const WS_PROXY_PROPERTY* proxyProperties,
//    _In_ const ULONG proxyPropertyCount,
//    _Outptr_ WS_SERVICE_PROXY** proxy,
//    _In_opt_ WS_ERROR* error);
//
// This function can be called from application code to create a service
// proxy object. A service proxy must be created before any request can be sent 
// to the web service.
//
// Also, the file may contain declaration of the following function:
//
//    HRESULT DefaultBinding_ICalculator_CreateServiceEndpoint(
//    _In_ WS_HTTP_SSL_BINDING_TEMPLATE* templateValue,
//    _In_opt_ WS_STRING address,
//    _In_opt_ struct DefaultBinding_ICalculatorFunctionTable* functionTable,
//    _In_opt_ WS_SERVICE_SECURITY_CALLBACK authorizationCallback,
//    _In_reads_opt_(endpointPropertyCount) WS_SERVICE_ENDPOINT_PROPERTY* endpointProperties,
//    _In_ const ULONG endpointPropertyCount,
//    _In_ WS_HEAP* heap,
//    _Outptr_ WS_SERVICE_ENDPOINT** serviceEndpoint,
//    _In_opt_ WS_ERROR* error);
//
// This function can be used to create a Service Host object, which has to be 
// created before a web services starts receiving requests from its clients.
//
// For more information about content of this file, please see the documentation for
// Wsutil compiler.
#if _MSC_VER > 1000 
#pragma once
#endif

#ifdef __cplusplus
extern "C" {
#endif

// The following client functions were generated:

//     BasicHttpBinding_ISocialGameService_Login
//     BasicHttpBinding_ISocialGameService_GetTagForUserName
//     BasicHttpBinding_ISocialGameService_GetUserProfiles
//     BasicHttpBinding_ISocialGameService_SendScores
//     BasicHttpBinding_ISocialGameService_GetFriendships
//     BasicHttpBinding_ISocialGameService_GetUsers
//     BasicHttpBinding_ISocialGameService_GetNumberOfFriendships

// The following server function tables were generated:

//     struct BasicHttpBinding_ISocialGameServiceFunctionTable

// the following policy helpers were generated:

//  BasicHttpBinding_ISocialGameService_CreateServiceProxy;
//  BasicHttpBinding_ISocialGameService_CreateServiceEndpoint;

// The following header files must be included in this order before this one

// #include <WebServices.h>
// #include "tempuri.org.xsd.h"

////////////////////////////////////////////////
// Policy helper routines
////////////////////////////////////////////////

//  WS_CHANNEL_PROPERTY_ENCODING = WS_ENCODING_XML_UTF8,
//  WS_CHANNEL_PROPERTY_ADDRESSING_VERSION = WS_ADDRESSING_VERSION_TRANSPORT,
//  WS_ENVELOPE_VERSION = WS_ENVELOPE_VERSION_SOAP_1_1,
// client channel type: WS_CHANNEL_TYPE_REQUEST, service endpoint channel type: WS_CHANNEL_TYPE_REPLY

HRESULT BasicHttpBinding_ISocialGameService_CreateServiceProxy(
    _In_opt_ WS_HTTP_BINDING_TEMPLATE* templateValue,
    _In_reads_opt_(proxyPropertyCount) const WS_PROXY_PROPERTY* proxyProperties,
    _In_ const ULONG proxyPropertyCount,
    _Outptr_ WS_SERVICE_PROXY** _serviceProxy,
    _In_opt_ WS_ERROR* error);

struct BasicHttpBinding_ISocialGameServiceFunctionTable;
HRESULT BasicHttpBinding_ISocialGameService_CreateServiceEndpoint(
    _In_opt_ WS_HTTP_BINDING_TEMPLATE* templateValue,
    _In_opt_ CONST WS_STRING* address,
    _In_opt_ struct BasicHttpBinding_ISocialGameServiceFunctionTable* functionTable,
    _In_opt_ WS_SERVICE_SECURITY_CALLBACK authorizationCallback,
    _In_reads_opt_(endpointPropertyCount) WS_SERVICE_ENDPOINT_PROPERTY* endpointProperties,
    _In_ const ULONG endpointPropertyCount,
    _In_ WS_HEAP* heap,
    _Outptr_ WS_SERVICE_ENDPOINT** serviceEndpoint,
    _In_opt_ WS_ERROR* error);

////////////////////////////////////////////////
// Client functions definitions
////////////////////////////////////////////////

// operation: BasicHttpBinding_ISocialGameService_Login
HRESULT WINAPI BasicHttpBinding_ISocialGameService_Login(
    _In_ WS_SERVICE_PROXY* _serviceProxy,
    _In_opt_z_ WCHAR* userName, 
    _In_opt_z_ WCHAR* password, 
    _Out_ int* LoginResult, 
    _In_ WS_HEAP* _heap,
    _In_reads_opt_(_callPropertyCount) const WS_CALL_PROPERTY* _callProperties,
    _In_ const ULONG _callPropertyCount,
    _In_opt_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_opt_ WS_ERROR* _error);

// operation: BasicHttpBinding_ISocialGameService_GetTagForUserName
HRESULT WINAPI BasicHttpBinding_ISocialGameService_GetTagForUserName(
    _In_ WS_SERVICE_PROXY* _serviceProxy,
    _In_ int authenticationKey, 
    _In_opt_z_ WCHAR* userName, 
    _Outptr_opt_result_z_ WCHAR** GetTagForUserNameResult, 
    _In_ WS_HEAP* _heap,
    _In_reads_opt_(_callPropertyCount) const WS_CALL_PROPERTY* _callProperties,
    _In_ const ULONG _callPropertyCount,
    _In_opt_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_opt_ WS_ERROR* _error);

// operation: BasicHttpBinding_ISocialGameService_GetUserProfiles
HRESULT WINAPI BasicHttpBinding_ISocialGameService_GetUserProfiles(
    _In_ WS_SERVICE_PROXY* _serviceProxy,
    _In_ int authenticationKey, 
    _Outptr_opt_result_z_ WCHAR** GetUserProfilesResult, 
    _In_ WS_HEAP* _heap,
    _In_reads_opt_(_callPropertyCount) const WS_CALL_PROPERTY* _callProperties,
    _In_ const ULONG _callPropertyCount,
    _In_opt_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_opt_ WS_ERROR* _error);

// operation: BasicHttpBinding_ISocialGameService_SendScores
HRESULT WINAPI BasicHttpBinding_ISocialGameService_SendScores(
    _In_ WS_SERVICE_PROXY* _serviceProxy,
    _In_ int authenticationKey, 
    _In_opt_z_ WCHAR* userName, 
    _In_ int numberOfFriends, 
    _In_ int strengthOfRelations, 
    _Out_ BOOL* SendScoresResult, 
    _In_ WS_HEAP* _heap,
    _In_reads_opt_(_callPropertyCount) const WS_CALL_PROPERTY* _callProperties,
    _In_ const ULONG _callPropertyCount,
    _In_opt_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_opt_ WS_ERROR* _error);

// operation: BasicHttpBinding_ISocialGameService_GetFriendships
HRESULT WINAPI BasicHttpBinding_ISocialGameService_GetFriendships(
    _In_ WS_SERVICE_PROXY* _serviceProxy,
    _In_ int authenticationKey, 
    _Outptr_opt_result_z_ WCHAR** GetFriendshipsResult, 
    _In_ WS_HEAP* _heap,
    _In_reads_opt_(_callPropertyCount) const WS_CALL_PROPERTY* _callProperties,
    _In_ const ULONG _callPropertyCount,
    _In_opt_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_opt_ WS_ERROR* _error);

// operation: BasicHttpBinding_ISocialGameService_GetUsers
HRESULT WINAPI BasicHttpBinding_ISocialGameService_GetUsers(
    _In_ WS_SERVICE_PROXY* _serviceProxy,
    _In_ int authenticationKey, 
    _Out_ int* GetUsersResult, 
    _In_ WS_HEAP* _heap,
    _In_reads_opt_(_callPropertyCount) const WS_CALL_PROPERTY* _callProperties,
    _In_ const ULONG _callPropertyCount,
    _In_opt_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_opt_ WS_ERROR* _error);

// operation: BasicHttpBinding_ISocialGameService_GetNumberOfFriendships
HRESULT WINAPI BasicHttpBinding_ISocialGameService_GetNumberOfFriendships(
    _In_ WS_SERVICE_PROXY* _serviceProxy,
    _In_ int authenticationKey, 
    _Out_ int* GetNumberOfFriendshipsResult, 
    _In_ WS_HEAP* _heap,
    _In_reads_opt_(_callPropertyCount) const WS_CALL_PROPERTY* _callProperties,
    _In_ const ULONG _callPropertyCount,
    _In_opt_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_opt_ WS_ERROR* _error);

////////////////////////////////////////////////
// Service functions definitions
////////////////////////////////////////////////

typedef HRESULT (CALLBACK* BasicHttpBinding_ISocialGameService_LoginCallback) (
    _In_ const WS_OPERATION_CONTEXT* _context,
    _In_opt_z_ WCHAR* userName, 
    _In_opt_z_ WCHAR* password, 
    _Out_ int* LoginResult, 
    _In_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_ WS_ERROR* _error);

typedef HRESULT (CALLBACK* BasicHttpBinding_ISocialGameService_GetTagForUserNameCallback) (
    _In_ const WS_OPERATION_CONTEXT* _context,
    _In_ int authenticationKey, 
    _In_opt_z_ WCHAR* userName, 
    _Outptr_opt_result_z_ WCHAR** GetTagForUserNameResult, 
    _In_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_ WS_ERROR* _error);

typedef HRESULT (CALLBACK* BasicHttpBinding_ISocialGameService_GetUserProfilesCallback) (
    _In_ const WS_OPERATION_CONTEXT* _context,
    _In_ int authenticationKey, 
    _Outptr_opt_result_z_ WCHAR** GetUserProfilesResult, 
    _In_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_ WS_ERROR* _error);

typedef HRESULT (CALLBACK* BasicHttpBinding_ISocialGameService_SendScoresCallback) (
    _In_ const WS_OPERATION_CONTEXT* _context,
    _In_ int authenticationKey, 
    _In_opt_z_ WCHAR* userName, 
    _In_ int numberOfFriends, 
    _In_ int strengthOfRelations, 
    _Out_ BOOL* SendScoresResult, 
    _In_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_ WS_ERROR* _error);

typedef HRESULT (CALLBACK* BasicHttpBinding_ISocialGameService_GetFriendshipsCallback) (
    _In_ const WS_OPERATION_CONTEXT* _context,
    _In_ int authenticationKey, 
    _Outptr_opt_result_z_ WCHAR** GetFriendshipsResult, 
    _In_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_ WS_ERROR* _error);

typedef HRESULT (CALLBACK* BasicHttpBinding_ISocialGameService_GetUsersCallback) (
    _In_ const WS_OPERATION_CONTEXT* _context,
    _In_ int authenticationKey, 
    _Out_ int* GetUsersResult, 
    _In_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_ WS_ERROR* _error);

typedef HRESULT (CALLBACK* BasicHttpBinding_ISocialGameService_GetNumberOfFriendshipsCallback) (
    _In_ const WS_OPERATION_CONTEXT* _context,
    _In_ int authenticationKey, 
    _Out_ int* GetNumberOfFriendshipsResult, 
    _In_ const WS_ASYNC_CONTEXT* _asyncContext,
    _In_ WS_ERROR* _error);

// binding: BasicHttpBinding_ISocialGameService
typedef struct BasicHttpBinding_ISocialGameServiceFunctionTable 
{
    BasicHttpBinding_ISocialGameService_LoginCallback BasicHttpBinding_ISocialGameService_Login;
    BasicHttpBinding_ISocialGameService_GetTagForUserNameCallback BasicHttpBinding_ISocialGameService_GetTagForUserName;
    BasicHttpBinding_ISocialGameService_GetUserProfilesCallback BasicHttpBinding_ISocialGameService_GetUserProfiles;
    BasicHttpBinding_ISocialGameService_SendScoresCallback BasicHttpBinding_ISocialGameService_SendScores;
    BasicHttpBinding_ISocialGameService_GetFriendshipsCallback BasicHttpBinding_ISocialGameService_GetFriendships;
    BasicHttpBinding_ISocialGameService_GetUsersCallback BasicHttpBinding_ISocialGameService_GetUsers;
    BasicHttpBinding_ISocialGameService_GetNumberOfFriendshipsCallback BasicHttpBinding_ISocialGameService_GetNumberOfFriendships;
} BasicHttpBinding_ISocialGameServiceFunctionTable;

////////////////////////////////////////////////
// Global web service descriptions.
////////////////////////////////////////////////

typedef struct _tempuri_org_wsdl
{
    struct // messages
    {
        // message: ISocialGameService_Login_InputMessage
        // c type: _Login
        // action: "http://tempuri.org/ISocialGameService/Login"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_Login_InputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_Login_InputMessage;
        
        // message: ISocialGameService_Login_OutputMessage
        // c type: _LoginResponse
        // action: "http://tempuri.org/ISocialGameService/LoginResponse"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_Login_OutputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_Login_OutputMessage;
        
        // message: ISocialGameService_GetTagForUserName_InputMessage
        // c type: _GetTagForUserName
        // action: "http://tempuri.org/ISocialGameService/GetTagForUserName"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetTagForUserName_InputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetTagForUserName_InputMessage;
        
        // message: ISocialGameService_GetTagForUserName_OutputMessage
        // c type: _GetTagForUserNameResponse
        // action: "http://tempuri.org/ISocialGameService/GetTagForUserNameResponse"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetTagForUserName_OutputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetTagForUserName_OutputMessage;
        
        // message: ISocialGameService_GetUserProfiles_InputMessage
        // c type: _GetUserProfiles
        // action: "http://tempuri.org/ISocialGameService/GetUserProfiles"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetUserProfiles_InputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetUserProfiles_InputMessage;
        
        // message: ISocialGameService_GetUserProfiles_OutputMessage
        // c type: _GetUserProfilesResponse
        // action: "http://tempuri.org/ISocialGameService/GetUserProfilesResponse"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetUserProfiles_OutputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetUserProfiles_OutputMessage;
        
        // message: ISocialGameService_SendScores_InputMessage
        // c type: _SendScores
        // action: "http://tempuri.org/ISocialGameService/SendScores"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_SendScores_InputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_SendScores_InputMessage;
        
        // message: ISocialGameService_SendScores_OutputMessage
        // c type: _SendScoresResponse
        // action: "http://tempuri.org/ISocialGameService/SendScoresResponse"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_SendScores_OutputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_SendScores_OutputMessage;
        
        // message: ISocialGameService_GetFriendships_InputMessage
        // c type: _GetFriendships
        // action: "http://tempuri.org/ISocialGameService/GetFriendships"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetFriendships_InputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetFriendships_InputMessage;
        
        // message: ISocialGameService_GetFriendships_OutputMessage
        // c type: _GetFriendshipsResponse
        // action: "http://tempuri.org/ISocialGameService/GetFriendshipsResponse"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetFriendships_OutputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetFriendships_OutputMessage;
        
        // message: ISocialGameService_GetUsers_InputMessage
        // c type: _GetUsers
        // action: "http://tempuri.org/ISocialGameService/GetUsers"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetUsers_InputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetUsers_InputMessage;
        
        // message: ISocialGameService_GetUsers_OutputMessage
        // c type: _GetUsersResponse
        // action: "http://tempuri.org/ISocialGameService/GetUsersResponse"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetUsers_OutputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetUsers_OutputMessage;
        
        // message: ISocialGameService_GetNumberOfFriendships_InputMessage
        // c type: _GetNumberOfFriendships
        // action: "http://tempuri.org/ISocialGameService/GetNumberOfFriendships"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetNumberOfFriendships_InputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetNumberOfFriendships_InputMessage;
        
        // message: ISocialGameService_GetNumberOfFriendships_OutputMessage
        // c type: _GetNumberOfFriendshipsResponse
        // action: "http://tempuri.org/ISocialGameService/GetNumberOfFriendshipsResponse"
        // messageDescription: tempuri_org_wsdl.messages.ISocialGameService_GetNumberOfFriendships_OutputMessage
        WS_MESSAGE_DESCRIPTION ISocialGameService_GetNumberOfFriendships_OutputMessage;
        
    } messages;
    struct // contracts
    {
        // binding: BasicHttpBinding_ISocialGameService
        // portType: ISocialGameService
        // operation: BasicHttpBinding_ISocialGameService_Login
        //     input message: ISocialGameService_Login_InputMessage
        //     output message: ISocialGameService_Login_OutputMessage
        // operation: BasicHttpBinding_ISocialGameService_GetTagForUserName
        //     input message: ISocialGameService_GetTagForUserName_InputMessage
        //     output message: ISocialGameService_GetTagForUserName_OutputMessage
        // operation: BasicHttpBinding_ISocialGameService_GetUserProfiles
        //     input message: ISocialGameService_GetUserProfiles_InputMessage
        //     output message: ISocialGameService_GetUserProfiles_OutputMessage
        // operation: BasicHttpBinding_ISocialGameService_SendScores
        //     input message: ISocialGameService_SendScores_InputMessage
        //     output message: ISocialGameService_SendScores_OutputMessage
        // operation: BasicHttpBinding_ISocialGameService_GetFriendships
        //     input message: ISocialGameService_GetFriendships_InputMessage
        //     output message: ISocialGameService_GetFriendships_OutputMessage
        // operation: BasicHttpBinding_ISocialGameService_GetUsers
        //     input message: ISocialGameService_GetUsers_InputMessage
        //     output message: ISocialGameService_GetUsers_OutputMessage
        // operation: BasicHttpBinding_ISocialGameService_GetNumberOfFriendships
        //     input message: ISocialGameService_GetNumberOfFriendships_InputMessage
        //     output message: ISocialGameService_GetNumberOfFriendships_OutputMessage
        // contractDescription: tempuri_org_wsdl.contracts.BasicHttpBinding_ISocialGameService
        WS_CONTRACT_DESCRIPTION BasicHttpBinding_ISocialGameService;
        
    } contracts;
    struct // policies
    {
        // policy for binding: BasicHttpBinding_ISocialGameService
        // port name: BasicHttpBinding_ISocialGameService, binding: BasicHttpBinding_ISocialGameService, namespace: http://tempuri.org/
        WS_HTTP_POLICY_DESCRIPTION BasicHttpBinding_ISocialGameService;
    } policies;
} _tempuri_org_wsdl;

extern const _tempuri_org_wsdl tempuri_org_wsdl;

#ifdef __cplusplus
}
#endif

