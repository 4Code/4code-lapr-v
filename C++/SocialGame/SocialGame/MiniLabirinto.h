#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Labirinto{
	public:
		/* VARIAVEIS GLOBAIS */
		typedef struct {
			GLboolean		up;
			GLboolean		down;
			GLboolean		left;
			GLboolean		right;
		}Teclas;

		typedef struct {
			GLboolean   doubleBuffer;
			GLint       delay;
			Teclas		teclas;
		}Estado;

		typedef struct {
			GLint		x;
			GLint		y;
		}Point;

		typedef struct {
			GLint		altura;
			GLint       largura;
			Point		posicao;
		}Modelo;

		Labirinto();

		Estado estado;
		Modelo modelo;
		char **labirinto;
		Point inicio;
		Point fim;
};
void MiniLabirinto_Init(void);

Labirinto::Labirinto(){
	MiniLabirinto_Init();
}

Labirinto miniLabironto;


/* Inicializa��o do ambiente OPENGL */
void MiniLabirinto_Init(void)
{
	//delay para o timer
	miniLabironto.estado.delay=100;

	string line;
	ifstream myfile ("lab.lab");
	if (!myfile.is_open()){
		cout << "Erro ao abrir o ficheiro\n";
		return;
	}
	getline (myfile,line);
	miniLabironto.modelo.altura = stoi(line);
	getline (myfile,line);
	miniLabironto.modelo.largura = stoi(line);

	miniLabironto.labirinto = (char**) malloc(miniLabironto.modelo.altura*sizeof(char*));
	for(int i=0; i<miniLabironto.modelo.altura; i++){
		miniLabironto.labirinto[i] = new char[miniLabironto.modelo.largura];

		getline (myfile,line);
		for(int j=0; j<miniLabironto.modelo.largura; j++){
			miniLabironto.labirinto[i][j] = line[j];
			if(line[j] == 'i'){
				miniLabironto.modelo.posicao.x = j;
				miniLabironto.modelo.posicao.y = i;
			}
		}
	}
	myfile.close();


	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);

}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void MiniLabirinto_Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint) width, (GLint) height);


	// Matriz Projec��o
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	gluOrtho2D(0, miniLabironto.modelo.largura, miniLabironto.modelo.altura, 0);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformac�es dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// ... definicao das rotinas auxiliares de desenho ...

void MiniLabirinto_desenhaCubo(GLfloat r, GLfloat g, GLfloat b){
	glBegin(GL_POLYGON);
	glColor3f( r, g, b );
	glVertex3f( 1, 0, 0 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( 0, 1, 0 );
	glVertex3f( 1, 1, 0 );
	glEnd();
}

void MiniLabirinto_desenhaLabirinto(){

	glPushMatrix();

	for(int x = 0; x < miniLabironto.modelo.largura; x++){
		for(int y = 0; y < miniLabironto.modelo.altura; y++){
			if(miniLabironto.labirinto[y][x] == 'x'){
				glPushMatrix();
				glTranslatef(x,y,0);
				MiniLabirinto_desenhaCubo(0,0,1);
				glPopMatrix();
			}
			if(miniLabironto.labirinto[y][x] == 'i'){ //inicio
				glPushMatrix();
				glTranslatef(x,y,0);
				MiniLabirinto_desenhaCubo(1,0,0);
				glPopMatrix();
			}
			if(miniLabironto.labirinto[y][x] == 'f'){ //fim
				glPushMatrix();
				glTranslatef(x,y,0);
				MiniLabirinto_desenhaCubo(0,1,0);
				glPopMatrix();
			}
		}
	}
	glPopMatrix();

}

void MiniLabirinto_desenhaJogador(){
	glPushMatrix();
	glTranslatef(miniLabironto.modelo.posicao.x,miniLabironto.modelo.posicao.y,0);
	MiniLabirinto_desenhaCubo(0.5,0.5,0.5);
	glPopMatrix();
}

// Callback de desenho

void MiniLabirinto_Draw(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	MiniLabirinto_desenhaLabirinto();
	MiniLabirinto_desenhaJogador();

	glFlush();
	if (miniLabironto.estado.doubleBuffer)
		glutSwapBuffers();
}

// ... definicao das rotinas de modelo ...

void MiniLabirinto_Vitoria(int value)
{
	printf("\n!!!! Parabens ganhou o jogo !!!!\n");
	Sleep(1000);
	exit(0);
}

GLboolean MiniLabirinto_detectaColisao(GLint nx,GLint ny){
	/*for(int y=0; y<modelo.altura; y++){
		for(int x=0; x<modelo.largura; x++){
			cout << labirinto[y][x];
		}
		cout << endl;
	}

	printf("labirinto[%d][%d]==%c\n",ny,nx,labirinto[ny][nx]);*/
	if(nx<0 || ny<0 || nx>=miniLabironto.modelo.largura || ny>=miniLabironto.modelo.altura){
		return GL_TRUE;
	}
	if(miniLabironto.labirinto[ny][nx]=='f'){
		miniLabironto.modelo.posicao.x = nx;
		miniLabironto.modelo.posicao.y = ny;
		glutTimerFunc(200, MiniLabirinto_Vitoria, 0);
		return GL_TRUE;
	}
	if(miniLabironto.labirinto[ny][nx]=='x'){
		return GL_TRUE;
	}
	return GL_FALSE;
}

// Callback de temporizador

void MiniLabirinto_Timer(int value)
{
	glutTimerFunc(miniLabironto.estado.delay, MiniLabirinto_Timer, 0);
	// ... accoes do temporizador ... 
	
	if(miniLabironto.estado.teclas.up == GL_TRUE){
		GLint ny = miniLabironto.modelo.posicao.y - 1;
		if(!MiniLabirinto_detectaColisao(miniLabironto.modelo.posicao.x, ny)){
			miniLabironto.modelo.posicao.y = ny;
		}
		PlLabirintoMain(miniLabironto.modelo.posicao.x, miniLabironto.modelo.posicao.y, miniLabironto.fim.x, miniLabironto.fim.y);
	}
	if(miniLabironto.estado.teclas.down == GL_TRUE){
		GLint ny = miniLabironto.modelo.posicao.y + 1;
		if(!MiniLabirinto_detectaColisao(miniLabironto.modelo.posicao.x, ny)){
			miniLabironto.modelo.posicao.y = ny;
		}
		PlLabirintoMain(miniLabironto.modelo.posicao.x, miniLabironto.modelo.posicao.y, miniLabironto.fim.x, miniLabironto.fim.y);
	}
	if(miniLabironto.estado.teclas.left == GL_TRUE){
		GLint nx = miniLabironto.modelo.posicao.x - 1;
		if(!MiniLabirinto_detectaColisao(nx, miniLabironto.modelo.posicao.y)){
			miniLabironto.modelo.posicao.x = nx;
		}
		PlLabirintoMain(miniLabironto.modelo.posicao.x, miniLabironto.modelo.posicao.y, miniLabironto.fim.x, miniLabironto.fim.y);
	}
	if(miniLabironto.estado.teclas.right == GL_TRUE){
		GLint nx = miniLabironto.modelo.posicao.x + 1;
		if(!MiniLabirinto_detectaColisao(nx, miniLabironto.modelo.posicao.y)){
			miniLabironto.modelo.posicao.x = nx;
		}
		PlLabirintoMain(miniLabironto.modelo.posicao.x, miniLabironto.modelo.posicao.y, miniLabironto.fim.x, miniLabironto.fim.y);
	}

	// redesenhar o ecra 
	glutPostRedisplay(); 
}


void MiniLabirinto_imprime_ajuda(void)
{
	printf("Seta para a esquerda - Andar para a esquerda\n");
	printf("Seta para cima - Andar para cima\n");
	printf("Seta para a direita - Andar para a direita\n");
	printf("Seta para baixo - Andar para baixo\n");
	printf("ESC - Sair\n");
}

/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interac��o via teclado (carregar na tecla)

void MiniLabirinto_Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 'h' :
	case 'H' :
		MiniLabirinto_imprime_ajuda();
		break;
	case 27 :
		exit(1);
		break;
	default:
		break;
	}
}

void MiniLabirinto_SpecialKey(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP:
		miniLabironto.estado.teclas.up = GL_TRUE;
		break;
	case GLUT_KEY_DOWN:
		miniLabironto.estado.teclas.down = GL_TRUE;
		break;
	case GLUT_KEY_LEFT:
		miniLabironto.estado.teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_RIGHT:
		miniLabironto.estado.teclas.right = GL_TRUE;
		break;
	}
}

void MiniLabirinto_SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP:
		miniLabironto.estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN:
		miniLabironto.estado.teclas.down = GL_FALSE;
		break;
	case GLUT_KEY_LEFT:
		miniLabironto.estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_RIGHT:
		miniLabironto.estado.teclas.right = GL_FALSE;
		break;
	}
}

void MiniLabirinto_Prolog(){
	ofstream myfile;
	myfile.open ("labirinto.pl");
	myfile << "/*http://www.swi-prolog.org/pldoc/doc_for?object=section(2,'F.1',swi('/doc/Manual/predsummary.html'))*/\n";
	myfile << ":-dynamic liga/2.\n";
	myfile << "liga2(I,F):-liga(I,F);liga(F,I).\n";
	myfile << "\n";
	myfile << "criaLabirinto(I,F):-assertz(liga(I,F)).\n";
	myfile << "\n";
	myfile << "pista([(Xp,_),(Xi,_)|_], Pista):- Xp > Xi, Pista = 'Esquerda', !.\n";
	myfile << "pista([(Xp,_),(Xi,_)|_], Pista):- Xp < Xi, Pista = 'Direita', !.\n";
	myfile << "pista([(_,Yp),(_,Yi)|_], Pista):- Yp > Yi, Pista = 'Cima', !.\n";
	myfile << "pista([(_,Yp),(_,Yi)|_], Pista):- Yp < Yi, Pista = 'Baixo', !.\n";
	myfile << "\n";
	myfile << "\n";
	myfile << "labirinto((Xi,Yi),(Xf,Yf),Pista):- lab((Xf,Yf),[[(Xi,Yi)]],Caminho), pista(Caminho,Pista).\n";
	myfile << "lab((Xf,Yf), [[(Xf,Yf)|Temp]|_], [(Xf,Yf)|Temp]):- !.\n";
	myfile << "lab((Xf,Yf), [[(Xi,Yi)|Temp]|Outros], Caminho):-\n";
	myfile << "	findall([(Xaux,Yaux),(Xi,Yi)|Temp], (liga2((Xi,Yi),(Xaux,Yaux)), not(member((Xaux,Yaux), Temp))), L),\n";
	myfile << "	append(Outros,L,LTemp),\n";
	myfile << "	lab((Xf,Yf), LTemp, Caminho).\n";
	myfile << "\n";
	myfile << "\n";


	for(int a = 0; a < miniLabironto.modelo.altura; a++){
		for(int l = 0; l < miniLabironto.modelo.largura; l++){
			if(miniLabironto.labirinto[a][l] != 'x'){
				if(a!=miniLabironto.modelo.altura-1 && miniLabironto.labirinto[a+1][l] != 'x'){
					myfile << "liga((" << l << "," << a << "),(" << l << "," << a+1 << ")).\n";
				}
				if(l!=miniLabironto.modelo.largura-1 && miniLabironto.labirinto[a][l+1] != 'x'){
					myfile << "liga((" << l << "," << a << "),(" << l+1 << "," << a << ")).\n";
				}
			}
			if(miniLabironto.labirinto[a][l] == 'i'){
				miniLabironto.inicio.x = l;
				miniLabironto.inicio.y = a;
			}
			if(miniLabironto.labirinto[a][l] == 'f'){
				miniLabironto.fim.x = l;
				miniLabironto.fim.y = a;
			}
		}
	}

	myfile.close();
}

int MiniLabirinto_main(int argc, char **argv)
{
	MiniLabirinto_Prolog();
	MiniLabirinto_Init();

	miniLabironto.estado.doubleBuffer=GL_TRUE;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(20*miniLabironto.modelo.largura, 20*miniLabironto.modelo.altura);
	glutInitDisplayMode(((miniLabironto.estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);
	if (glutCreateWindow("Labirinto") == GL_FALSE)
		exit(0);

	

	MiniLabirinto_imprime_ajuda();

	// Registar callbacks do GLUT

	// callbacks de janelas/desenho
	glutReshapeFunc(MiniLabirinto_Reshape);
	glutDisplayFunc(MiniLabirinto_Draw);

	// Callbacks de teclado
	glutKeyboardFunc(MiniLabirinto_Key);
	//glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(MiniLabirinto_SpecialKey);
	glutSpecialUpFunc(MiniLabirinto_SpecialKeyUp);

	glutTimerFunc(miniLabironto.estado.delay, MiniLabirinto_Timer, 0);


	// COMECAR...
	glutMainLoop();

	return 0;
}