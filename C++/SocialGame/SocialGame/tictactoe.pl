interface(A,B,C,D,E,F,G,H,I,RETURN):- cpuplay([A,B,C,D,E,F,G,H,I],RETURN).







%Começar jogo. Irá ser apresentado o guião do jogo logo de entrada.

playtictactoe :- guide, board([-,-,-,-,-,-,-,-,-]).

%Guia do jogo

guide :- write('To begin the game you need to enter a number followed by a dot, from the board below, in order to replace it with your "X".'),nl, showBoard([1,2,3,4,5,6,7,8,9]).

%Display do estado actual do tabuleiro

showBoard([A,B,C,D,E,F,G,H,I]) :- write([A,B,C]),nl,write([D,E,F]),nl, write([G,H,I]),nl,nl.

% Este predicado lê as jogadas do user e do cpu, assim como o resultado
% delas.

board(Board) :- win(Board, x), write('Congratulations, You win!').
board(Board) :- win(Board, o), write('Sorry, I win!').
board(Board) :- read(Int), userplay(Board, Int, Newboard), showBoard(Newboard), cpuplay(Newboard, Nextboard),
	showBoard(Nextboard), board(Nextboard).

% Verifica se e como é que o jogo foi ganho(coluna, linha,
% diagonal) e anuncia o vencedor, enviando a resposta para o predicado
% game.
% Nota: Turn = x ou o

win(Board, Turn) :- row(Board, Turn).
win(Board, Turn) :- collumn(Board, Turn).
win(Board, Turn) :- diagonal(Board, Turn).

row(Board, Turn) :- Board = [Turn,Turn,Turn,
			     _,_,_
			    ,_,_,_].

row(Board, Turn) :- Board = [_,_,_,
			    Turn,Turn,Turn,
			     _,_,_].

row(Board, Turn) :- Board = [_,_,_,
			     _,_,_,
			     Turn,Turn,Turn].

collumn(Board, Turn) :- Board = [Turn,_,_,
				 Turn,_,_
				,Turn,_,_].

collumn(Board, Turn) :- Board = [_,Turn,_,
				 _,Turn,_,
				 _,Turn,_].

collumn(Board, Turn) :- Board = [_,_,Turn,
				 _,_,Turn,
				 _,_,Turn].

diagonal(Board, Turn) :- Board = [Turn,_,_,
				  _,Turn,_,
				  _,_,Turn].

diagonal(Board, Turn) :- Board = [_,_,Turn,
				  _,Turn,_,
				  Turn,_,_].


% Esta é a transfomação que irá ocorrer na jogada. Quando o utilizador
% escolhe um inteiro, este é substituido por "x", enquanto que o cpu
% utiliza "o".

userplay([-,B,C,D,E,F,G,H,I], 1, [x,B,C,D,E,F,G,H,I]).
userplay([A,-,C,D,E,F,G,H,I], 2, [A,x,C,D,E,F,G,H,I]).
userplay([A,B,-,D,E,F,G,H,I], 3, [A,B,x,D,E,F,G,H,I]).
userplay([A,B,C,-,E,F,G,H,I], 4, [A,B,C,x,E,F,G,H,I]).
userplay([A,B,C,D,-,F,G,H,I], 5, [A,B,C,D,x,F,G,H,I]).
userplay([A,B,C,D,E,-,G,H,I], 6, [A,B,C,D,E,x,G,H,I]).
userplay([A,B,C,D,E,F,-,H,I], 7, [A,B,C,D,E,F,x,H,I]).
userplay([A,B,C,D,E,F,G,-,I], 8, [A,B,C,D,E,F,G,x,I]).
userplay([A,B,C,D,E,F,G,H,-], 9, [A,B,C,D,E,F,G,H,x]).
userplay(Board,_, Board) :- write('Illegal move.'), nl.

cpuplay(Board,Newboard) :- playo(Board, o, Newboard), win(Newboard, o),!.
cpuplay(Board,Newboard) :- playo(Board, o, Newboard).
cpuplay(Board,Newboard) :- not(member(-,Board)),!, write('Draw'), nl, Newboard = Board.

playo([-,B,C,D,E,F,G,H,I], Turn, [Turn,B,C,D,E,F,G,H,I]).
playo([A,-,C,D,E,F,G,H,I], Turn, [A,Turn,C,D,E,F,G,H,I]).
playo([A,B,-,D,E,F,G,H,I], Turn, [A,B,Turn,D,E,F,G,H,I]).
playo([A,B,C,-,E,F,G,H,I], Turn, [A,B,C,Turn,E,F,G,H,I]).
playo([A,B,C,D,-,F,G,H,I], Turn, [A,B,C,D,Turn,F,G,H,I]).
playo([A,B,C,D,E,-,G,H,I], Turn, [A,B,C,D,E,Turn,G,H,I]).
playo([A,B,C,D,E,F,-,H,I], Turn, [A,B,C,D,E,F,Turn,H,I]).
playo([A,B,C,D,E,F,G,-,I], Turn, [A,B,C,D,E,F,G,Turn,I]).
playo([A,B,C,D,E,F,G,H,-], Turn, [A,B,C,D,E,F,G,H,Turn]).


