﻿
function getTagForUserName(userName) {
   
    $.ajax({
        url: urlGetTagForUserName + "?userName=" + userName,
        type: "get",
        datatype: "json",
        success: function(result) {
            clearTagCloud();
            for (var i = 0; i < result.length ; i++) {
                appendToTagCloud(result[i].Item1, result[i].Item2);
            }
            refreshTagCloud();
        },
        error: function(e) {
            alert(e);
        }
    });
}

function getFriendshipTagForUserName(userName) {

    $.ajax({
        url: urlGetFriendshipTagForUserName + "?userName=" + userName,
        type: "get",
        datatype: "json",
        success: function (result) {
            clearTagCloud();
            for (var i = 0; i < result.length ; i++) {
                appendToTagCloud(result[i].Item1, result[i].Item2);
            }
            refreshTagCloud();
        },
        error: function (e) {
            alert(e);
        }
    });
}

function getTags() {

    $.ajax({
        url: urlGetTags,
        type: "get",
        datatype: "json",
        success: function (result) {
            clearTagCloud();
            for (var i = 0; i < result.length ; i++) {
                appendToTagCloud(result[i].Item1, result[i].Item2);
            }
            refreshTagCloud();
        },
        error: function (e) {
            alert(e);
        }
    });
}

    function getRelationshipTags() {

        $.ajax({
            url: urlGetRelationshipTags,
            type: "get",
            datatype: "json",
            success: function (result) {
                clearTagCloud();
                for (var i = 0; i < result.length ; i++) {
                    appendToTagCloud(result[i].Item1, result[i].Item2);
                }
                refreshTagCloud();
            },
            error: function (e) {
                alert(e);
            }
        });
    }

    

function appendToTagCloud(text, size) {
    var tagList = document.getElementById('tagList');
    var newListItem = document.createElement('li');
    tagList.appendChild(newListItem);
    newListItem.innerHTML = "<a href='#'>" + text + "</a>";
    newListItem.style.fontSize = size + 'px';

}

function clearTagCloud() {
    var tagList = document.getElementById('tagList');
    tagList.innerHTML = "";
}

function refreshTagCloud() {
    if (!$('#myCanvas').tagcanvas({
        textColour: '#00f',
        outlineThickness: 1,
        maxSpeed: 0.03,
        depth: 0.75,
        weight: true,
        textFont: '"Arial Black",sans-serif',

    })) {
        // TagCanvas failed to load
        $('#myCanvasContainer').hide();
    }
}
