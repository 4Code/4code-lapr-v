﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SocialGame.Web.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
using SocialGame.Web.Abstract;
using System.Data;
using WebMatrix.WebData;
using System.Web;


namespace SocialGame.Web.DAL
{
    public class SocialGameDb : DbContext, ISocialGameDataSource
    {
        public SocialGameDb()
            : base("SocialGameDb")
        {
        }

        public DbSet<Friendship> Friendships { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Mood> Moods { get; set; }
        public DbSet<LeaderBoard> LeaderBoards { get; set; }

        IQueryable<Friendship> ISocialGameDataSource.Friendships
        {
            get { return Friendships; }
        }

        IQueryable<Tag> ISocialGameDataSource.Tags
        {
            get { return Tags; }
        }

        IQueryable<UserProfile> ISocialGameDataSource.UserProfiles
        {
            get { return UserProfiles; }
        }

        IQueryable<Mood> ISocialGameDataSource.Moods
        {
            get { return Moods; }
        }

        IQueryable<LeaderBoard> ISocialGameDataSource.LeaderBoards
        {
            get { return LeaderBoards; }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public UserProfile GetUserProfileByID(int id)
        {
            return this.UserProfiles.Find(id);
        }

        public void UpdateUserProfile(UserProfile userProfile)
        {
            this.Entry(userProfile).State = EntityState.Modified;
        }

        public void DeleteFriendship(Friendship friendship)
        {
            this.Entry(friendship).State = EntityState.Deleted;
        }

        public void UpdateFriendship(Friendship friendship)
        {
            this.Entry(friendship).State = EntityState.Modified;
        }

        public void CreateFriendship(Friendship friendship)
        {
            this.Entry(friendship).State = EntityState.Added;
        }

        public void AddUserLeaderBoard(LeaderBoard leaderboard)
        {
            this.Entry(leaderboard).State = EntityState.Added;
        }

        public void Save()
        {
            this.SaveChanges();
        }

        public string CheckLogin(string userName, string password)
        {
            string result = "false";
            
            try
            {
                SimpleMembershipProvider provider = new SimpleMembershipProvider();
                
                var name = provider.GetUser(userName, false);

                if(name.GetPassword().Equals(password))
                {
                    result = "true";
                }
            }
            catch(System.Exception ex)
            {
                return ex.ToString();
            }

            return result;
        }
    }
}
