﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialGame.Web.Enum
{
    public enum RelationType
    {
        Unknown = 1,
        Acquaintance = 2,
        Colleague = 3,
        Friend = 4,
        Family = 5
    }
}