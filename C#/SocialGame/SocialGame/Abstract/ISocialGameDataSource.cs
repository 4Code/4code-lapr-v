﻿using SocialGame.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialGame.Web.Abstract
{
    public interface ISocialGameDataSource
    {
        IQueryable<UserProfile> UserProfiles { get; }
        IQueryable<Friendship> Friendships { get; }
        IQueryable<Tag> Tags { get; }
        IQueryable<Mood> Moods { get; }
        IQueryable<LeaderBoard> LeaderBoards { get; }
        UserProfile GetUserProfileByID(int userProfileId);

        string CheckLogin(string userName, string password);
        void AddUserLeaderBoard(LeaderBoard leaderboard);
        void UpdateUserProfile(UserProfile userProfile);
        void DeleteFriendship(Friendship friendship);
        void UpdateFriendship(Friendship friendship);
        void CreateFriendship(Friendship friendship);
        void Save();
    }
}
