using SocialGame.Web.Abstract;
using SocialGame.Web.DAL;
using StructureMap;
namespace SocialGame.Web
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
                            x.For<ISocialGameDataSource>().HttpContextScoped().Use<SocialGameDb>();
                        });
            return ObjectFactory.Container;
        }
    }
}