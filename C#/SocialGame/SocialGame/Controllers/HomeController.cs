﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace SocialGame.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }       

        public ActionResult SetLanguage(string languageCode)
        {
            HttpContext.Session["culture"] = languageCode;
            return RedirectToAction("Index");
        }
    }
}
