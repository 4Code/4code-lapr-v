﻿using SocialGame.Web.Abstract;
using SocialGame.Web.Enum;
using SocialGame.Web.Models;
using SocialGame.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace SocialGame.Web.Controllers
{
    public class FriendshipController : Controller
    {
        ISocialGameDataSource db;

        public FriendshipController(ISocialGameDataSource db)
        {
            this.db = db;
        }

        public ActionResult Index(string userName)
        {
            FriendshipViewModel friendshipViewModel = new FriendshipViewModel();

            friendshipViewModel.FriendRequests = new List<UserProfile>();

            friendshipViewModel.MyUserProfile = this.db.UserProfiles.Where(u => u.UserName == userName).FirstOrDefault();

            if(friendshipViewModel.MyUserProfile != null)
            {
                List<Friendship> friendships = this.db.Friendships.Where(f => f.UserProfileOneId == friendshipViewModel.MyUserProfile.UserId || f.UserProfileTwoId == friendshipViewModel.MyUserProfile.UserId).ToList();

                List<int> friendsUserIds = new List<int>();
                List<int> friendRequestsUserIds = new List<int>();

                foreach(Friendship friend in friendships)
                {
                    if(friend.IsFriend)
                    {
                        if(friend.UserProfileOneId != friendshipViewModel.MyUserProfile.UserId)
                        {
                            friendsUserIds.Add(friend.UserProfileOneId);
                        }
                        else
                        {
                            friendsUserIds.Add(friend.UserProfileTwoId);
                        }
                    }
                    else
                    {
                        if(friend.UserProfileTwoId == friendshipViewModel.MyUserProfile.UserId)
                        {
                            friendRequestsUserIds.Add(friend.UserProfileOneId);
                        }
                    }
                }

                friendshipViewModel.MyFriends = this.db.UserProfiles.Where(u => friendsUserIds.Contains(u.UserId)).ToList();
                friendshipViewModel.FriendRequests = this.db.UserProfiles.Where(u => friendRequestsUserIds.Contains(u.UserId)).ToList();
            }

            return View(friendshipViewModel);
        }

        public ActionResult IgnoreFriendRequest(int friendUserId, int myUserId, string myUserName)
        {
            Friendship friendship = this.db.Friendships.Where(f => f.UserProfileOneId == friendUserId && f.UserProfileTwoId == myUserId).FirstOrDefault();

            if(friendship != null)
            {
                this.db.DeleteFriendship(friendship);
                this.db.Save();
            }

            return RedirectToAction("Index", new { userName = myUserName });
        }

        public ActionResult DeleteFriend(int friendUserId, int myUserId, string myUserName)
        {
            Friendship friendship = this.db.Friendships.Where(f => f.UserProfileOneId == friendUserId && f.UserProfileTwoId == myUserId || f.UserProfileTwoId == friendUserId && f.UserProfileOneId == myUserId).FirstOrDefault();

            if(friendship != null)
            {
                this.db.DeleteFriendship(friendship);
                this.db.Save();
            }

            return RedirectToAction("Index", new { userName = myUserName });
        }

        public ActionResult AcceptFriendRequest(int friendUserId, int myUserId, string myUserName)
        {
            FriendshipRelationViewModel viewModel = new FriendshipRelationViewModel();

            viewModel.MyUserId = myUserId;
            viewModel.FriendUserId = friendUserId;
            viewModel.MyUserName = myUserName;

            viewModel.RelationTypes = new List<Enum.RelationType>();
            viewModel.RelationTypes.Add(Enum.RelationType.Unknown);
            viewModel.RelationTypes.Add(Enum.RelationType.Acquaintance);
            viewModel.RelationTypes.Add(Enum.RelationType.Colleague);
            viewModel.RelationTypes.Add(Enum.RelationType.Friend);
            viewModel.RelationTypes.Add(Enum.RelationType.Family);

            return View("ViewFriendshipRelation", viewModel);           
        }

        public ActionResult ConfirmFriendshipRelation(int friendUserId, int myUserId, string myUserName, RelationType relationType)
        {
            Friendship friendship = this.db.Friendships.Where(f => f.UserProfileOneId == friendUserId && f.UserProfileTwoId == myUserId).FirstOrDefault();

            if(friendship != null)
            {
                friendship.IsFriend = true;
                friendship.Strength = (int)relationType;
                this.db.UpdateFriendship(friendship);
                this.db.Save();
            }

            return RedirectToAction("Index", new { userName = myUserName });
        }

        public ActionResult ViewUserProfile(int friendUserId, int myUserId)
        {
            FriendshipViewModel friendshipViewModel = new FriendshipViewModel();

            friendshipViewModel.MyFriendRequestProfile = this.db.UserProfiles.Where(u => u.UserId == friendUserId).FirstOrDefault();

            friendshipViewModel.MyUserProfile = this.db.UserProfiles.Where(u => u.UserId == myUserId).FirstOrDefault();

            return View(friendshipViewModel);
        }

        public ActionResult ViewFriendProfile(int friendUserId, int myUserId)
        {
            FriendshipViewModel friendshipViewModel = new FriendshipViewModel();

            friendshipViewModel.MyFriendProfile = this.db.UserProfiles.Where(u => u.UserId == friendUserId).FirstOrDefault();

            friendshipViewModel.MyUserProfile = this.db.UserProfiles.Where(u => u.UserId == myUserId).FirstOrDefault();

            return View(friendshipViewModel);
        }

        public ActionResult ViewMyFriendFriends(int friendUserId, int myUserId)
        {
            FriendshipViewModel friendshipViewModel = new FriendshipViewModel();

            friendshipViewModel.MyFriendProfile = this.db.UserProfiles.Where(u => u.UserId == friendUserId).FirstOrDefault();

            friendshipViewModel.MyUserProfile = this.db.UserProfiles.Where(u => u.UserId == myUserId).FirstOrDefault();

            List<Friendship> friendships = this.db.Friendships.Where(f => f.UserProfileOneId == friendshipViewModel.MyFriendProfile.UserId || f.UserProfileTwoId == friendshipViewModel.MyFriendProfile.UserId).ToList();

            List<Friendship> myFriendships = this.db.Friendships.Where(f => f.UserProfileOneId == friendshipViewModel.MyUserProfile.UserId || f.UserProfileTwoId == friendshipViewModel.MyUserProfile.UserId).ToList();

            List<int> friendsUserIds = new List<int>();

            List<int> myFriendsUserIds = new List<int>();

            foreach(Friendship friend in friendships)
            {
                if(friend.IsFriend)
                {
                    if(friend.UserProfileOneId != friendshipViewModel.MyFriendProfile.UserId)
                    {
                        friendsUserIds.Add(friend.UserProfileOneId);
                    }
                    else
                    {
                        friendsUserIds.Add(friend.UserProfileTwoId);
                    }
                }
            }

            foreach(Friendship friend in myFriendships)
            {
                if(friend.IsFriend)
                {
                    if(friend.UserProfileOneId != friendshipViewModel.MyUserProfile.UserId)
                    {
                        myFriendsUserIds.Add(friend.UserProfileOneId);
                    }
                    else
                    {
                        myFriendsUserIds.Add(friend.UserProfileTwoId);
                    }
                }
            }

            friendshipViewModel.MyFriends = this.db.UserProfiles.Where(u => myFriendsUserIds.Contains(u.UserId)).ToList();
            friendshipViewModel.MyFriendFriends = this.db.UserProfiles.Where(u => friendsUserIds.Contains(u.UserId)).ToList();

            return View(friendshipViewModel);
        }

        public ActionResult SearchFriends(string searchSetting, string userName)
        {
            FriendSearchViewModel searchViewModel = new FriendSearchViewModel();

            searchViewModel.MyUserProfile = this.db.UserProfiles.Where(u => u.UserName == userName).FirstOrDefault();

            List<Friendship> myFriendships = this.db.Friendships.Where(f => f.UserProfileOneId == searchViewModel.MyUserProfile.UserId || f.UserProfileTwoId == searchViewModel.MyUserProfile.UserId).ToList();
            List<string> myFriendshipProfilesUsernames = new List<string>();

            foreach(Friendship friendship in myFriendships)
            {
                if(friendship.UserProfileOneId == searchViewModel.MyUserProfile.UserId)
                {
                    myFriendshipProfilesUsernames.Add(this.db.UserProfiles.Where(u => u.UserId == friendship.UserProfileTwoId).FirstOrDefault().UserName);
                }
                else
                {
                    myFriendshipProfilesUsernames.Add(this.db.UserProfiles.Where(u => u.UserId == friendship.UserProfileOneId).FirstOrDefault().UserName);
                }
            }

            IQueryable<UserProfile> userProfileQuery = this.db.UserProfiles.Where(u => u.UserName.Contains(searchSetting)
                || (u.FirstName != null && u.FirstName.Contains(searchSetting))
                || (u.LastName != null && u.LastName.Contains(searchSetting)));

            userProfileQuery = userProfileQuery.Where(u => u.UserName != userName);

            userProfileQuery = userProfileQuery.Where(u => !myFriendshipProfilesUsernames.Contains(u.UserName));

            searchViewModel.SearchResults = userProfileQuery.Distinct().ToList();

            return PartialView("_FriendSearch", searchViewModel);
        }

        public ActionResult SendFriendRequest(int friendUserId, int myUserId)
        {
            FriendSearchViewModel friendSearchViewModel = new FriendSearchViewModel();

            friendSearchViewModel.MyUserProfile = this.db.UserProfiles.Where(u => u.UserId == myUserId).FirstOrDefault();

            Friendship newFriendship = new Friendship();

            newFriendship.UserProfileOneId = myUserId;
            newFriendship.UserProfileTwoId = friendUserId;
            newFriendship.IsFriend = false;

            this.db.CreateFriendship(newFriendship);
            this.db.Save();

            return PartialView("_FriendSearch", friendSearchViewModel);
        }

    }
}
