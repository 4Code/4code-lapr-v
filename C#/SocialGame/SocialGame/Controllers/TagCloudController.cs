﻿using SocialGame.Web.Abstract;
using SocialGame.Web.DAL;
using SocialGame.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SocialGame.Web.Controllers
{
    public class TagCloudController : Controller
    {
        private ISocialGameDataSource db;

        public TagCloudController(ISocialGameDataSource db)
        {
            this.db = db;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetTagForUserName(string userName)
        {
            List<Tuple<string, int>> result = new List<Tuple<string, int>>();

            UserProfile userProfile = this.db.UserProfiles.Where(p => p.UserName == userName).FirstOrDefault();

            if(userProfile != null && userProfile.Tags != null)
            {
                List<Tag> tags = userProfile.Tags.ToList();

                tags.ForEach(t => result.Add(new Tuple<string, int>(t.Description, 10)));
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFriendshipTagForUserName(string userName)
        {
            List<Tuple<string, int>> result = new List<Tuple<string, int>>();

            List<Tag> tags = new List<Tag>();

            UserProfile userProfile = this.db.UserProfiles.Where(p => p.UserName == userName).FirstOrDefault();

            List<Friendship> friendships = this.db.Friendships.Where(f => f.UserProfileOneId == userProfile.UserId || f.UserProfileTwoId == userProfile.UserId).ToList();

            foreach(Friendship friendship in friendships)
            {
                tags.AddRange(friendship.Tags.ToList());
            }

            List<Tag> distinctTags = tags.Distinct().ToList();

            distinctTags.ForEach(t => result.Add(new Tuple<string, int>(t.Description, tags.Count(y => y.Description == t.Description) * 8)));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTags()
        {
            List<Tuple<string, int>> result = new List<Tuple<string, int>>();
            List<Tag> tags = new List<Tag>();

            List<UserProfile> userProfiles = this.db.UserProfiles.ToList();

            foreach(UserProfile userProfile in userProfiles)
            {
                tags.AddRange(userProfile.Tags.ToList());
            }

            List<Tag> distinctTags = tags.Distinct().ToList();

            distinctTags.ForEach(t => result.Add(new Tuple<string, int>(t.Description, tags.Count(y => y.Description == t.Description) * 8)));

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult GetRelationshipTags()
        {
            List<Tuple<string, int>> result = new List<Tuple<string, int>>();
            List<Tag> tags = new List<Tag>();

            List<Friendship> friendships = this.db.Friendships.ToList();

            foreach(Friendship friendship in friendships)
            {
                tags.AddRange(friendship.Tags.ToList());
            }

            List<Tag> distinctTags = tags.Distinct().ToList();

            distinctTags.ForEach(t => result.Add(new Tuple<string, int>(t.Description, tags.Count(y => y.Description == t.Description) * 8)));

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}