﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialGame.Web.Models;
using SocialGame.Web.DAL;
using SocialGame.Web.Abstract;

namespace SocialGame.Web.Controllers
{
    public class UserProfileController : Controller
    {
        private ISocialGameDataSource db;

        public UserProfileController(ISocialGameDataSource db)
        {
            this.db = db;
        }

        /// <summary>
        /// Index View 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string userName)
        {
            UserProfile userProfile = this.db.UserProfiles.Where(p => p.UserName == userName).FirstOrDefault();

            if(userProfile != null)
            {
                return View(userProfile);
            }
            else
            {
                return View(new UserProfile());
            }
        }

        public ActionResult Edit(int id = 0)
        {
            //UserProfile userProfile = db.GetUserProfileByID(id);
            UserProfile userProfile = this.db.UserProfiles.Where(p => p.UserId == id).FirstOrDefault();
            if(userProfile == null)
            {
                return HttpNotFound();
            }
            ViewBag.MoodId = new SelectList(this.db.Moods, "MoodID", "Description", userProfile.MoodId);
            
            return View(userProfile);
        }


        [HttpPost]
        public ActionResult Edit(UserProfile userProfile)
        {
            if(ModelState.IsValid)
            {
                this.db.UpdateUserProfile(userProfile);
                this.db.Save();
                return RedirectToAction("Index", userProfile);
            }
            return View(userProfile);
        }
    }
}