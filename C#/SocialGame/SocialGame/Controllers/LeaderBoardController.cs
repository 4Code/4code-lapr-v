﻿using SocialGame.Web.Abstract;
using SocialGame.Web.Models;
using SocialGame.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SocialGame.Web.Controllers
{
    public class LeaderBoardController : Controller
    {
        private ISocialGameDataSource db;

        public LeaderBoardController(ISocialGameDataSource db)
        {
            this.db = db;
        }

        public ActionResult Index(string username)
        {
            username = string.IsNullOrEmpty(username) ? string.Empty : username;

            LeaderBoardViewModel viewModel = new LeaderBoardViewModel();

            viewModel.RelationLeaderBoard = new List<Tuple<string, int>>();

            List<LeaderBoard> relationList = this.db.LeaderBoards.OrderByDescending(x => x.friendPoints).Distinct().Take(10).ToList();

            relationList.ForEach(r => viewModel.RelationLeaderBoard.Add(new Tuple<string, int>(r.userName, r.friendPoints)));

            if(!string.IsNullOrEmpty(username))
            {
                LeaderBoard myLeaderBoard = this.db.LeaderBoards.Where(l => l.userName == username).OrderByDescending(x => x.friendPoints).FirstOrDefault();

                if(myLeaderBoard != null)
                {
                    viewModel.RelationLeaderBoard.Add(new Tuple<string, int>(myLeaderBoard.userName, myLeaderBoard.friendPoints));
                }
            }

            viewModel.StrengthLeaderBoard = new List<Tuple<string, int>>();

            List<LeaderBoard> strengthList = this.db.LeaderBoards.OrderByDescending(x => x.strengthPoints).Distinct().Take(10).ToList();

            strengthList.ForEach(r => viewModel.StrengthLeaderBoard.Add(new Tuple<string, int>(r.userName, r.strengthPoints)));

            if(!string.IsNullOrEmpty(username))
            {
                LeaderBoard myLeaderBoard = this.db.LeaderBoards.Where(l => l.userName == username).OrderByDescending(x => x.strengthPoints).FirstOrDefault();

                if(myLeaderBoard != null)
                {
                    viewModel.StrengthLeaderBoard.Add(new Tuple<string, int>(myLeaderBoard.userName, myLeaderBoard.strengthPoints));
                }
            }

            return View(viewModel);
        }


    }
}
