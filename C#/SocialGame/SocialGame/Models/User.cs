﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialGame.Web.Models
{
    public class User
    {
        public virtual int UserID { get; set; }       
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; }
        public virtual string Address { get; set; }
        public virtual string Country { get; set; }
        public virtual string City { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string BirthDate { get; set; }
        public virtual string Telephone { get; set; }

        public virtual ICollection<Friendship> Friendships { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
