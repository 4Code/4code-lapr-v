﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialGame.Web.Models
{
    public class Friendship
    {

        public virtual int FriendshipId { get; set; }
        [Display(Name = "Strength", ResourceType = typeof(Resources.Resources))]
        public virtual int? Strength { get; set; }
        public virtual int UserProfileOneId { get; set; }
        public virtual int UserProfileTwoId { get; set; }
        public virtual bool IsFriend { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}
