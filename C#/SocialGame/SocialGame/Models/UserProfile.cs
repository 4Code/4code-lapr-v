﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialGame.Web.Models
{
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public virtual int UserId { get; set; }
        public virtual string UserName { get; set; }

        [StringLength(20, ErrorMessageResourceName = "StringLengthMaxMin", MinimumLength = 3, ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resources))]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessageResourceName = "InvalidName", ErrorMessageResourceType = typeof(Resources.Resources))]
        public virtual string FirstName { get; set; }

        [StringLength(20, ErrorMessageResourceName = "StringLengthMaxMin", MinimumLength = 3, ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "LastName", ResourceType = typeof(Resources.Resources))]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessageResourceName = "InvalidName", ErrorMessageResourceType = typeof(Resources.Resources))]
        public virtual string LastName { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessageResourceName = "InvalidEmailAddress", ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "Email", ResourceType = typeof(Resources.Resources))]
        public virtual string Email { get; set; }

        [StringLength(200, ErrorMessageResourceName = "StringLengthMaxMin", MinimumLength = 5, ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "Address", ResourceType = typeof(Resources.Resources))]
        public virtual string Address { get; set; }

        [StringLength(20, ErrorMessageResourceName = "StringLengthMaxMin", MinimumLength = 3, ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "Country", ResourceType = typeof(Resources.Resources))]
        [RegularExpression("^[a-zA-Z ]*$",ErrorMessageResourceName="InvalidName",ErrorMessageResourceType=typeof(Resources.Resources))]
        public virtual string Country { get; set; }

        [StringLength(20, ErrorMessageResourceName = "StringLengthMaxMin", MinimumLength = 3, ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "City", ResourceType = typeof(Resources.Resources))]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessageResourceName = "InvalidName", ErrorMessageResourceType = typeof(Resources.Resources))]
        public virtual string City { get; set; }
        public virtual string ZipCode { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "BirthDate", ResourceType = typeof(Resources.Resources))]
        public virtual string BirthDate { get; set; }

        [Display(Name = "Telephone", ResourceType = typeof(Resources.Resources))]
        [RegularExpression("[0-9]+", ErrorMessage = "Only numbers allowed")]
        public virtual string Telephone { get; set; }

        public virtual int? MoodId { get; set; }
        public virtual Mood Mood { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}