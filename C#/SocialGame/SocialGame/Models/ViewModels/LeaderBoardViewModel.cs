﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialGame.Web.Models.ViewModels
{
    public class LeaderBoardViewModel
    {
        public List<Tuple<string, int>> StrengthLeaderBoard { get; set; }

        public List<Tuple<string, int>> RelationLeaderBoard { get; set; }
    }
}