﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialGame.Web.Models.ViewModels
{
    public class FriendshipViewModel
    {
        public UserProfile MyUserProfile { get; set; }

        public List<UserProfile> MyFriends { get; set; }

        public List<UserProfile> MyFriendFriends { get; set; }

        public List<UserProfile> FriendRequests { get; set; }

        public UserProfile FriendSuggestion { get; set; }

        public UserProfile MyFriendProfile { get; set; }

        public UserProfile MyFriendRequestProfile { get; set; }
    }
}