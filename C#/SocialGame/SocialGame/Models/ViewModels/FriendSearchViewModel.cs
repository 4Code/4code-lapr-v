﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialGame.Web.Models.ViewModels
{
    public class FriendSearchViewModel
    {
        public UserProfile MyUserProfile { get; set; }

        public string SearchSetting { get; set; }

        public List<UserProfile> SearchResults { get; set; }
    }
}