﻿using SocialGame.Web.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialGame.Web.Models.ViewModels
{
    public class FriendshipRelationViewModel
    {
        public int MyUserId { get; set; }
        
        public int FriendUserId { get; set; }

        public string MyUserName { get; set; }

        public List<RelationType> RelationTypes { get; set; }

        public RelationType SelectedRelationType { get; set; }
    }
}