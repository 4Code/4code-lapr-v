﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SocialGame.Web.Models
{
    public class Mood
    {
        public virtual int MoodId { get; set; }

        [Display(Name = "Mood", ResourceType = typeof(Resources.Resources))]
        public virtual string Description { get; set; }

    }
}
