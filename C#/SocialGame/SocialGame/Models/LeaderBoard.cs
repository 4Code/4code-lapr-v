﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialGame.Web.Models
{
    public class LeaderBoard
    {
        public virtual int LeaderBoardId { get; set; }
        public virtual int strengthPoints { get; set; }
        public virtual int friendPoints { get; set; }
        public virtual string userName { get; set; }
    }
}