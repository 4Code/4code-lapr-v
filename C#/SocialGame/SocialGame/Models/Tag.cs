﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialGame.Web.Models
{
    public class Tag
    {
        public virtual int TagId { get; set; }
        
        [StringLength(20, ErrorMessageResourceName = "StringLengthMaxMin", MinimumLength = 2, ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "NameOfTag", ResourceType = typeof(Resources.Resources))]
        public virtual string Description { get; set; }

        public virtual ICollection<Friendship> Friendships { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
