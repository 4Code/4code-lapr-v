﻿using SocialGame.Web.Abstract;
using SocialGame.Web.DAL;
using SocialGame.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WebMatrix.WebData;
using WebMatrix.Data;
using System.Configuration;
using System.Web.Security;

namespace SocialGame.Services
{
    public class SocialGameService : ISocialGameService
    {
        private ISocialGameDataSource db;

        public int Login(string userName, string password)
        {
            this.db = new SocialGameDb();

            if(!password.Equals("password"))
            {
                return 0;
            }

            UserProfile userProfile = this.db.UserProfiles.Where(u => u.UserName == userName).FirstOrDefault();

            if(userProfile != null)
            {
                return 1;
            }

            return 0;

        }

        public int getUsers(int authenticationKey)
        {
            this.db = new SocialGameDb();

            List<UserProfile> userProfiles = this.db.UserProfiles.ToList();

            int countUsers = userProfiles.Count();

            return countUsers;
        }

        public string GetTagForUserName(int authenticationKey, string userName)
        {
            if(authenticationKey != 4444)
            {
                string fail = "authenticationKey";

                return fail;
            }

            UserProfile userProfile = this.db.UserProfiles.Where(u => u.UserName == userName).FirstOrDefault();

            List<Tag> tags = userProfile.Tags.ToList();

            string result = "";

            foreach(Tag tag in tags)
            {
                result += tag.Description;
                result += ",";
            }

            return result;
        }

        public string GetFriendships(int authenticationKey)
        {
            if(authenticationKey != 4444)
            {
                string fail = "authenticationKey";

                return fail;
            }

            List<Friendship> friendships = this.db.Friendships.ToList();

            string result = "";

            foreach(Friendship friendship in friendships)
            {
                result += friendship.UserProfileOneId;
                result += ";";
                result += friendship.UserProfileTwoId;
                result += ";";
                result += friendship.Strength;
                result += ",";
            }

            return result;
        }

        public string GetUserProfiles(int authenticationKey)
        {
            if(authenticationKey != 4444)
            {
                string fail = "authenticationKey";

                return fail;
            }

            StringBuilder result = new StringBuilder();

            try
            {
                this.db = new SocialGameDb();

                List<UserProfile> profileList = this.db.UserProfiles.ToList();

                foreach(UserProfile profile in profileList)
                {
                    result.Append(profile.UserName);
                    result.Append(",");
                }
            }
            catch(System.Exception ex)
            {
                return ex.ToString();
            }

            return result.ToString();
        }



        public bool SendScores(int authenticationKey, string userName, int numberOfFriends, int strengthOfRelations)
        {
            bool result = false;

            this.db = new SocialGameDb();

            if(authenticationKey == 4444)
            {
                result = true;
                LeaderBoard leaderboard = new LeaderBoard();
                leaderboard.userName = userName;
                leaderboard.friendPoints = numberOfFriends;
                leaderboard.strengthPoints = strengthOfRelations;
                this.db.AddUserLeaderBoard(leaderboard);
                this.db.Save();
            }

            return result;
        }
    }
}
