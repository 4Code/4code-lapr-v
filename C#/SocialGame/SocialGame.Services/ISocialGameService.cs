﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SocialGame.Services
{
    [ServiceContract]
    public interface ISocialGameService
    {
        [OperationContract]
        int Login(string userName, string password);

        [OperationContract]
        string GetTagForUserName(int authenticationKey, string userName);

        [OperationContract]
        string GetUserProfiles(int authenticationKey);

        [OperationContract]
        bool SendScores(int authenticationKey, string userName, int numberOfFriends, int strengthOfRelations);

        [OperationContract]
        string GetFriendships(int authenticationKey);

        [OperationContract]
        int GetUsers(int authenticationKey);
    }

}
